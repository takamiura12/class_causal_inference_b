# 入力データ
DATA = [
    "A,167,71,58,135",
    "B,160,79,53,111",
    "C,178,83,34,133",
    "D,173,66,20,129",
    "E,176,82,57,122",
    "F,167,85,44,94",
    "G,160,80,51,106",
    "H,151,82,44,132",
    "I,153,65,41,108",
    "J,169,58,56,91"
]

class HealthReport:

    def __init__(self, data_str):
        self.data = self.parse_text(data_str) # 文字列をリストに分解

    def parse_text(self, data_str):
        return data_str.split(",")

    def get_user_id(self):
        return self.data[0]

    # 身長・体重から BMI を計算する
    def calc_BMI(self):
        height = int(self.data[1]) # 文字列型を整数型に変換
        weight = int(self.data[2])
        bmi = weight / (height / 100) ** 2
        return round(bmi, 2) # 小数点 2 桁丸めた結果を返す

    # BMI から肥満度のカテゴリー分けをする
    def classify_BMI(self):
        bmi = self.calc_BMI()
        if bmi > 25:
            return "Overweight"

        elif bmi < 18.5:
            return "Underweight"
        
        else:
            return "Normal"


    # 個人ID, BMI, 肥満度の辞書型を出力する
    def make(self):
        return {
            "ID": self.get_user_id(),
            "BMI": self.calc_BMI(),
            "Obesity": self.classify_BMI(),
        }

results = []
for data_str in DATA:
    report = HealthReport(data_str).make() # 診断レポートを出力
    results.append(report) # 診断レポートを追加
    
print(results[:3]) # 結果の一部を出力