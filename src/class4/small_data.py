
# 入力データ
DATA = [
    "A,167,71,58,135",
    "B,160,79,53,111",
    "C,178,83,34,133",
    "D,173,66,20,129",
    "E,176,82,57,122",
    "F,167,85,44,94",
    "G,160,80,51,106",
    "H,151,82,44,132",
    "I,153,65,41,108",
    "J,169,58,56,91"
]


def calc_BMI(data):
    height = int(data[1]) # 文字列型を整数型に変換
    weight = int(data[2])
    bmi = weight / (height / 100) ** 2
    return round(bmi, 2) # 小数点 2 桁丸めた結果を返す

def classify_BMI(bmi):
    if bmi > 25:
        return "Overweight"

    elif bmi < 18.5:
        return "Underweight"
    
    else:
        return "Normal"

def make_report(data_str):
    data = data_str.split(",") # 文字列をリストに分割

    # 身長・体重から BMI を計算する
    bmi = calc_BMI(data) 

    # BMI から肥満度のカテゴリー分けをする
    obesity = classify_BMI(bmi)

    # 個人ID, BMI, 肥満度の辞書型を作成する
    user_id = data[0]
    report = {
        "ID": user_id,
        "BMI": bmi,
        "Obesity": obesity
    }
    
    # 診断レポートを返す
    return report

# 1. リストから要素を取り出す
# 6. 処理を繰り返す
# 7. 結果をリストにまとめる
results = []
for data_str in DATA:
    report = make_report(data_str)
    results.append(report) # 診断レポートを追加

print(results[:3]) # 結果の一部を出力
