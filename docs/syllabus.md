
## アウトライン

1.	Python の基礎①		
2.	Python の基礎②		
3.	Python の基礎③		
4.	Pandas によるデータ操作		
5.	データ分析①		
6.	データ分析②		
7.	データ分析③		
8.	まとめ

## 成績評価

- 小テスト: 20%
- 課題: 40%
- 最終レポート: 40%

# ねらい

この講義では、因果推論 A で学んだ基礎的な手法およびデータ分析のスキルを Python というプログラミング言語を用いて学んでいきます。
Python の基礎的な方法を学んだあと、検定や回帰分析の方法を学び、基礎的な分析のスキルを身につけます。
また、データの前処理やグラフの作成などについても学んでいきます。			

# 到達目標
- Python で 1 通りのデータ分析ができる

# 教科書
- 特にありません。Moodle 上で資料を配布します。

# 参考書・参考文献
- 中妻照雄 『Pythonによる計量経済学入門 (実践Pythonライブラリー)』 朝倉書店 2020年
- Wes McKinney 『Pythonによるデータ分析入門 第3版 ―pandas、NumPy、Jupyterを使ったデータ処理』オライリー・ジャパン 2023年

# 履修上の注意・メッセージ
- 資料配布・課題提出・小テストの解答などには Moodle を使用します。
- 小テストは講義開始時に行います。

# 関連科目
  
# 指示

# 連絡事項