
# グラフ作成

ここでは、`plotnine` というライブラリを使って、Python でグラフを作成する方法を学んでいきます。

## なぜグラフを作るのか

そもそも、なぜグラフを作る必要があるのでしょうか。
例えば、以下は[`e-stat`](https://www.e-stat.go.jp/stat-search/files?page=1&layout=datalist&toukei=00400001&tstat=000001011528&cycle=0&tclass1=000001021812&tclass2val=0) から取得した、2014 年から 2023 年度の大学生の数の遷移を示したものです。

```py
年度    大学生数
2014  2,855,529
2015  2,860,210
2016  2,873,624
2017  2,890,880
2018  2,909,159
2019  2,918,668
2020  2,915,605
2021  2,917,998
2022  2,930,780
2023  2,945,599
```

このデータから大学生の数は変遷を辿っているか読み取ってください。
数は増えていますか。
そのスピードはどのくらいですか。
このくらいであれば、少し時間があれば読み解けるかと思います。

さて、このデータを以下のようなグラフにしたらどうでしょうか。

![](../../assets/class8/student_line.png)

先ほどの数値データを見るよりも、楽に傾向をつかむことができたのではないでしょうか。
これがグラフの力です。
つまり、グラフはデータに隠れているパターンを視覚的に表現するためのツールです。

## どのような時にグラフを使うのか

グラフを使う目的としては、主に 2 つ挙げられます。
1 つ目は、データを理解するためです。
例えば、以下のようなグラフ (ヒストグラム) は、データの値がどの範囲にどのくらいの頻度で表れているかを視覚的に表現しています。

先ほどの例に出したグラフも、時間経過に伴う変化を視覚的に表現しています。

> [!note]
> グラフや記述統計などを行い、データの特性を明らかにすることを **探索的データ分析** (Explanatory Data Aanalysis: EDA) といいます。
> 分析の前準備や仮説を見つけるために使います。

2 つ目は、ストーリーを語るためです。
例えば、ある年から景気が悪くなったとか、A という方法が他の方法よりも優れているといったものです。
こうしたグラフは、論文やプレゼンなどで使われ、著者が伝えたいメッセージを補強するために使われます。

どのような目的でグラフを作成するのかを念頭に置くことは非常に重要です。
データを理解するためのグラフに不必要なメッセージは、読者 (グラフを読み取る人) がデータの全体像を理解するのを阻害します。
同時に、そのようなメッセージの押し付けは、著者に対する不信感につながります。
一方で、解釈が必要なグラフをただ示すだけだと、読者に解釈を任せるという、認知的な負荷を課してしまいます。

## 避けるべきグラフ

グラフが与える視覚的な効果は、非常に強力です。
しかし、強力な故に、実際とは異なる解釈に誘導することもできてしまいます。
例えば、先ほどのグラフでは大学生は毎年一定数増加しているというような印象を持ったと思います。

では、次はどうでしょう。
よく見ると、2019 年から 2021 年にかけて少し減少していることがわかります。
直近 5 年のデータだけを使い、y 軸の範囲を変えてみます。

![](../../assets/class8/invalid_graph.png)

2021 年から急激に大学生が増えているように見えませんか。
もしそのような説明がついていれば、さらにそのように見えると思います。
このように、グラフの作り方によっては実際よりもストーリーを強めたり弱めたりすることができてしまいます。
データ分析においてもそうですが、グラフを作成するときにずるをしないという倫理的な意識を持つことが重要です。
逆に、読者側の時にはグラフに騙されない心掛けが重要です。

また、たまに見かけるのですが、何のグラフなのかを最低限示さないとせっかく作ったグラフも意味がなくなってしまいます。

![](../../assets/class8/less_info.png)

> [!tip] 
> タイトル、縦軸・横軸のラベル全てを書く必要はありませんが、最低限このグラフだけを見て何を示しているわかるようにしましょう。

## `plotnine` 導入

### Python のグラフ作成ライブラリ

正直なところ、Python でグラフを作成するのは R よりも難しいと言えます。
Python で基本とされているグラフ作成ライブラリは [`matplotlib`](https://matplotlib.org/) というもので、これは MATLAB という数値計算用のプログラミング言語でできるグラフのようなものを作れるようになっています。
`matplotlib` は多機能で、3D のグラフなども作成できる反面、その構造が非常に難解です。
そのため、意図するグラフを作成するには、根気がいります。
`matplotlib` をベースにした [`seaborn`](https://seaborn.pydata.org/) といった、`pandas` から "それっぽい" グラフができるライブラリもあります。
しかし、`seaborn` は細かい調整ができず、結局 `matplotlib` のドキュメントを探る必要があります。

一方で、R は [`ggplot2`](https://ggplot2.tidyverse.org/) というグラフ用のライブラリが中心となっています。
また、`ggplot2` は細かい体裁を整えることも容易に可能です。

[`plotnine`](https://plotnine.readthedocs.io/en/v0.12.4/) は、この `ggplot2` を Python で実装することを目的としています。
まだ一部機能は実装されていませんが、すでに多くのことが行えます。
また、若干の文法の違いはあるものの、`ggplot2` を使った例が本やブログなどにあるため、それらを参考にすることができます。

### インストール

まずは `plotnine` を Python にインストールします。

```bash
pip install plotnine
```

ライブラリ名もそのまま `plotnine` です。

```py
>>> import plotnine
```

> [!note]
> `plotnine` 全体をインポートすると、少し時間がかかるため、`from ~ import` で使う機能のみをインポートするのが多いようです。
> また、`plotnine` 自体をインポートする場合は `p9` と略す人もいるようです (私は `pn` と略して使っています)。

### チュートリアル

`plotnine` で用意されているサンプルのデータを用いて、車両の重さ (`wt`: weight) と 燃費 (`mpg`: miles per gallon) の関係を表す散布図を作成してみます。

```py
>>> from plotnine import ggplot, geom_point, aes
>>> from plotnine.data import mtcars
>>> ggplot(data = mtcars, mapping = aes(x = "wt", y = "mpg")) + geom_point()
```

次のグラフが出力されるはずです。

![](../../assets/class8/tutorial.png)

### `plotnine` の基本

`ggplot2` のデザインを踏襲している `plotnine` で基本となるのは次の 3 つです。

- `ggplot`: グラフのベース
- `geom`: グラフの種類
- `aes`: データとグラフの対応

[`ggplot`](https://plotnine.readthedocs.io/en/v0.12.4/generated/plotnine.ggplot.html) オブジェクトはグラフを作成する箱のようなものです。
このオブジェクトを作成するだけでは、グラフを作るという宣言をしただけなので、何のグラフを作るかを教える必要があります。

[`geom`](https://plotnine.readthedocs.io/en/stable/generated/plotnine.geoms.geom.geom.html) オブジェクトは、特定のグラフのオブジェクトを表しています。
各グラフは、`geom_xxx` という名前がついており、例えばヒストグラムは [`geom_histogram`](https://plotnine.readthedocs.io/en/stable/generated/plotnine.geoms.geom_histogram.html#plotnine.geoms.geom_histogram)、棒グラフは [`geom_bar`](https://plotnine.readthedocs.io/en/stable/generated/plotnine.geoms.geom_bar.html)、折れ線グラフは [`geom_line`](https://plotnine.readthedocs.io/en/stable/generated/plotnine.geoms.geom_line.html) です。

グラフを作成するには、もちろん元となるデータが必要です。
`plotnine` では `pandas` の `DataFrame` を用います。
また、データからグラフへの対応 (mapping) を `aes` オブジェクトを使って表現します。
例えば、グラフの x 軸, y 軸は `DataFrame` のどのカラムと対応するかといったものです。

## レイヤー構造

`plotnine` でグラフを作成するのは、デジタルツールでイラストを描くのに似ています。
デジタルツールでイラストを描くには、まずイラストを描く領域 (キャンバス) を用意します。
その上に線を描いたり、色を塗ったりします。
さらにその上に、層 (レイヤー) を重ねて色を載せたり、線をつけたりしていきます。

`ggplot` オブジェクトは、先の例のキャンバスに相当します。
`geom` オブジェクトが、その上に載せる具体的な絵のイメージです。
レイヤーを重ねるには、`+` 演算子を用います。

### `ggplot` インスタンスの作成

`ggplot` インスタンスを作成するには、以下の 3 つの方法があります。
チュートリアルで用いたサンプルデータ [`mtcars`](https://plotnine.readthedocs.io/en/stable/generated/plotnine.data.mtcars.html) の散布図を [`geom_point`](https://plotnine.readthedocs.io/en/stable/generated/plotnine.geoms.geom_point.html) で作成しなおします。

以下はサンプルデータの一部です。

```py
>>> from plotnine.data import mtcars
>>> mtcars.head()
                name   mpg  cyl   disp   hp  drat     wt   qsec  vs  am  gear  carb
0          Mazda RX4  21.0    6  160.0  110  3.90  2.620  16.46   0   1     4     4
1      Mazda RX4 Wag  21.0    6  160.0  110  3.90  2.875  17.02   0   1     4     4
2         Datsun 710  22.8    4  108.0   93  3.85  2.320  18.61   1   1     4     1
3     Hornet 4 Drive  21.4    6  258.0  110  3.08  3.215  19.44   1   0     3     1
4  Hornet Sportabout  18.7    8  360.0  175  3.15  3.440  17.02   0   0     3     2
```

1. `ggplot()` 

このパターンでは、とりあえずキャンバスだけ用意するイメージです。
用いるデータも、データとグラフの対応関係も指定していません。
ですので、以下のように `geom` オブジェクトでデータと対応関係を指定する必要があります。

```py
>>> from plotnine import ggplot, geom_point, aes
>>> from plotnine.data import mtcars
>>> ggplot() + geom_point(data = mtcars, mapping = aes(x = "wt", y = "mpg"))
```

2. `ggplot(<data>)`

このパターンでは、用いるデータだけ指定しています。
そこから、データとグラフの対応関係を、`geom` オブジェクトで指定します。

```py
>>> from plotnine import ggplot, geom_point, aes
>>> from plotnine.data import mtcars
>>> ggplot(data = mtcars) + geom_point(mapping = aes(x = "wt", y = "mpg"))
```

3. `ggplot(<data>, <mapping>)`

このパターンは、データとデータとグラフの対応関係を `ggplot` オブジェクト作成時に指定しています。
その後に、作成するグラフの種類の `geom` オブジェクトを足しています。

```py
>>> from plotnine import ggplot, geom_point, aes
>>> from plotnine.data import mtcars
>>> ggplot(data = mtcars, mapping = aes(x = "wt", y = "mpg")) + geom_point()
```

> [!note]
> 基本的にはどの方法でも構いませんが、グラフによってデータを変える必要があるか、対応関係を変える必要があるかで、どのパターンを採用するか変わってきます。
> 個人的には、使うデータだけ決めておく 2 のパターンを良く用いてます。

ちなみに、1. 2 のパターンで `ggplot` オブジェクトのみを作った場合は次のようになります。

![](../../assets/class8/only_canvas.png)

3 のパターンだと、x 軸と y 軸が加わっています。

![](../../assets/class8/canvas_with_labels.png)

### グラフの積み重ね

`ggplot` オブジェクトにはグラフにグラフを重ねることもできます。
例えば以下のように、散布図のグラフ `geom_point` に回帰直線のグラフ [`geom_smooth`](https://plotnine.readthedocs.io/en/stable/generated/plotnine.geoms.geom_smooth.html) で追加しています。 
`geom_smooth` の `method` キーワードを `lm` にすることで、回帰直線のグラフを作成します。

```py
>>> from plotnine import ggplot, geom_point, geom_smooth, aes
>>> from plotnine.data import mtcars
>>> (ggplot(data = mtcars, mapping = aes(x = "wt", y = "mpg")) 
... + geom_point()
... + geom_smooth())
```

![](../../assets/class8/with_lineplot.png)

このように、グラフの層を重ねることで、柔軟な表現が可能になります。

## 基本的なグラフの紹介

### 散布図: [`geom_point`](https://plotnine.readthedocs.io/en/stable/generated/plotnine.geoms.geom_point.html)

散布図は、2 つの項目のデータの位置 (x, y) をプロットしたものです。
2 つの項目にどのような関係があるかを可視化するのに向いています。
また、データの傾向や異常値の発見などにも用いられます。

### 棒グラフ: [`geom_bar`](https://plotnine.readthedocs.io/en/stable/generated/plotnine.geoms.geom_bar.html)

値の大きさを棒の高さ (長さ) で表現したグラフです。
例えば都道府県別のあるデータの値の比較など、カテゴリー間の値の大小関係を比較するのに用いられます。
または、カテゴリーの頻度を比較するのにも用いられます。

以下は、変速機のタイプ別の頻度を表しています。
`am` が変速機のタイプを表しており、`0` がオートマ車、`1` がミッション車です。

```py
>>> from plotnine import ggplot, geom_bar, aes
>>> from plotnine.data import mtcars
>>> ggplot(data = mtcars) + geom_bar(mapping = aes(x = "am"))
```

![](../../assets/class8/bar_plot.png)

x 軸の表記が少し違和感があります。
これは、`am` は整数型なので、x 軸が数直線になっているからです。
新しくカテゴリー変数を作っても良いですが、より簡単に `0` と `1` のカテゴリーにするには、`factor` という表記を用います。
`factor` は関数ではなく、文字列内で表記するにに注意してください。

```py
>>> from plotnine import ggplot, geom_bar, aes
>>> from plotnine.data import mtcars
>>> ggplot(data = mtcars) + geom_bar(mapping = aes(x = "factor(am)"))
```

![](../../assets/class8/factor_bar.png)

より自然な表記になったことが分かります。


### 折れ線グラフ: [`geom_line`](https://plotnine.readthedocs.io/en/stable/generated/plotnine.geoms.geom_line.html)

散布図の一種で、時系列データなど、連続的な変化のパターンを表現するのに用います。

### ヒストグラム: [`geom_histogram`](https://plotnine.readthedocs.io/en/stable/generated/plotnine.geoms.geom_histogram.html#plotnine.geoms.geom_histogram)

ある項目の値の分布を表すのに使います。
とりうる値の範囲を bin という区間 (例: 0 から 5 まで) に分け、その中で観測された頻度を棒の長さで表示します。 
以下は、燃費のヒストグラムを図示しています。

```py
>>> from plotnine import ggplot, geom_histogram, aes
>>> from plotnine.data import mtcars
>>> ggplot(data = mtcars) + geom_bar(mapping = aes(x = "mpg"))
```

![](../../assets/class8/histogram.png)

### 箱ひげ図: [`geom_boxplot`](https://plotnine.readthedocs.io/en/stable/generated/plotnine.geoms.geom_boxplot.html)

ある項目の記述統計量 (平均や分散) を可視化したグラフです。
ヒストグラムとは異なった形で、データの全体像を示してくれます。
また、カテゴリー間で比較することも多く用いられます。

下の図は、燃費の箱ひげ図を変速機のタイプ別に図示しています。

```py
>>> from plotnine import ggplot, geom_barplot, aes
>>> from plotnine.data import mtcars
>>> ggplot(data = mtcars) + geom_boxplot(mapping = aes(x = "factor(am)", y = "mpg"))
```

![](../../assets/class8/box_plot.png)

> [!tip]
> この場合は、`factor` を使って `am` をカテゴリー化しないとうまくいきません。
> 実際に試してみてください。

上の髭先が最大値、箱の上側が 75 パーセンタイル、太線が 50 パーセンタイル (中央値)、箱の下側が 25 パーセンタイル、下の髭先が最低値を表しています。

## 応用的なグラフ作成

### 直線: [`geom_hline`](https://plotnine.readthedocs.io/en/stable/generated/plotnine.geoms.geom_hline.html), [`geom_vline`](https://plotnine.readthedocs.io/en/stable/generated/plotnine.geoms.geom_vline.html), [`geom_abline`](https://plotnine.readthedocs.io/en/stable/generated/plotnine.geoms.geom_abline.html)

順に、x 軸 (横軸) に平行 (horizontal) な線 `geom_hline`、垂直 (vertical) な線 `geom_vline`、斜線 (slope: $y = ax + b$) `geom_abline` を引くことができます。
単体で用いることはほぼありませんが、特定の値を強調したりするときに使います。

例えば、次の例は散布図に燃費の平均を表す線をつけ足しています (`yintercept` キーワードを用います)。
これによって、平均以上かどうかの比較が容易になったのが見やすくなりました。

```py
>>> from plotnine import ggplot, geom_point, aes, geom_hline
>>> from plotnine.data import mtcars
(ggplot(data = mtcars, mapping = aes(x = "wt", y = "mpg")) 
+ geom_point() 
+ geom_hline(yintercept = mtcars["mpg"].mean()))
```

![](../../assets/class8/hline.png)

### ファセット: [`facet_wrap`](https://plotnine.readthedocs.io/en/stable/generated/plotnine.facets.facet_wrap.html)

カテゴリー毎にグラフを足し合わせることもできますが、グラフによってはカテゴリー毎にグラフを分けたいこともあります。
そのような場合、`facet_wrap` を用いることで、グラフを分割することができます。

例えば、以下は車両の前ギアの数によってデータの散布図のパターンがどう変わるかを示しています。
`facet_wrap` オブジェクト内に、カテゴリー分けするカラム名を指定します。

```py
>>> from plotnine import ggplot, geom_point, geom_smooth, aes, facet_wrap
>>> from plotnine.data import mtcars
>>> (ggplot(data = mtcars, mapping = aes(x = "wt", y = "mpg")) 
... + geom_point() 
... + facet_wrap("gear"))
```

![](../../assets/class8/facet_wrap.png)


> [!tip]
> グラフを分割する方法として [`facet_grid`](https://plotnine.readthedocs.io/en/stable/generated/plotnine.facets.facet_grid.html) というものもあります。 
> `facet_wrap` より柔軟な分割が可能ですが、分割の指定方法が少し難しいため、ここでは紹介しません。
> 興味がある人はリンクのページを見てみてください。
> また、`ggplot2` で使える一部の機能は現時点ではまだ実装されていないようです。

### 集計: [`stat_summary`](https://plotnine.readthedocs.io/en/stable/generated/plotnine.stats.stat_summary.html)

`plotnine` でグラフを作成する際に、データの集計も同時に行うこともできます。
棒グラフで変速機のタイプ別の燃費の平均を表したいとします。

`stat_summary` を使うことで、棒グラフを作る際にデータを集計してから渡すことができます。
この例以外にも、`stat_summary` で様々なことが可能ですが、y 軸の変数に適用する関数を指定するには `fun_y` キーワード、作成するグラフを `geom` キーワードで指定します。
平均を計算するために、`numpy` の `mean` 関数を使います。

コード例は次のようになります。

```py
>>> from plotnine import ggplot, aes, stat_summary
>>> import numpy as np
>>> (ggplot(data = mtcars, mapping = aes(x = "factor(am)", y = "mpg")) 
... + stat_summary(fun_y = np.mean, geom = "bar"))
```

![](../../assets/class8/stat_summary.png)

## カスタマイズ

### タイトル、軸ラベル

`labs` オブジェクトをつけ足すことで、タイトル、軸 (x, y) ラベルを変更できます。

```py
>>> from plotnine import ggplot, geom_point, aes, labs
>>> from plotnine.data import mtcars
>>> (ggplot(data = mtcars, mapping = aes(x = "wt", y = "mpg"))
... + geom_point()
... + labs(
...     title = "Scatter Plot of Vehicle Weight and Fuel Efficiency",
...     x = "Vehicle Weight (lb)",
...     y = "Fuel Efficiency (miles per gallon)")
... )
```

![](../../assets/class8/with_labs.png)

### テーマ

デフォルトでは `plotnine` のグラフの背景は薄いグレーに白い格子です。
`theme_xxx` という名前のテーマがいくつか用意されており、それをつけ足すことでテーマを変えることができます。

例えば、`theme_classic` は白背景に線だけのシンプルなものになります。

```py
>>> from plotnine import ggplot, geom_point, aes, theme_classic
>>> from plotnine.data import mtcars
>>> (ggplot(data = mtcars, mapping = aes(x = "wt", y = "mpg")) 
... + geom_point() 
... + theme_classic())
```

![](../../assets/class8/theme_classic.png)

使えるテーマはこのページから確認できます (テーマ一覧: https://plotnine.readthedocs.io/en/stable/api.html)。
とりあえず動かしてみて、良さそうなテーマを探してみてください。

さらに細かい調整は `theme` オブジェクトを使って行います。

> [!note]
> `theme` オブジェクトによる細かい修正は、正直なところやりながら覚えていくしかないと思います。
> `ggplot2` と実装が少し異なる部分があったり、ドキュメントもこの部分は整備されていないです。
> ただ、`matplotlib` を直接扱うよりも楽に調整が可能だと思います。

### テキスト

`matplotlib` もそうですが、デフォルトでは日本語をグラフに表示できません。
デフォルトのフォントは DejaVu Sans です (参考ページ: https://matplotlib.org/stable/users/explain/text/fonts.html)。
フォントが日本語に対応していないため、いわゆる豆腐文字 ▯ (英語でも tofu と呼ばれるようです) に置き換わってしまいます。
なので、日本語に対応しているフォントを指定する必要があります。
OS によって最初からインストールされているフォントが異なるのですが、Windows ではメイリオ、Mac OS では ヒラギノをとりあえず使います。

> [!tip]
> Windows OS では、設定 -> 個人用設定 -> フォントから、インストールされているフォントを見ることができます。
>
> Mac OS では、Font Book アプリケーションから確認できます。

まずは、フォントを変更しないままタイトルを日本語にしてみます。

```py
>>> from plotnine import ggplot, geom_point, aes, labs
>>> from plotnine.data import mtcars
>>> (ggplot(data = mtcars, mapping = aes(x = "wt", y = "mpg")) 
... + geom_point() 
... + labs(title = "車両重量と燃費の散布図"))
```

タイトルの文字が上手く表示されてないことが分かります。

![](../../assets/class8/tofu_graph.png)

`theme` オブジェクト内でテキストの設定を変更することができます。
テキストの設定は、`element_text` というオブジェクトを使う必要があります。
フォントファミリー (フォントの種類) を指定するには、次のようにします。

```py
element_text(family = <font_name>)
```

`<font_name>` はフォントファミリーの名前 (英語) です。
メイリオなら `"meiryo"`、ヒラギノなら `"Hiragino Sans"` (他もあります) です。

> [!tip]
> 先ほどのフォント一覧では、日本語名しか分かりません。
> フォントの英語名を調べるには、Goolge で検索した方が早いです。 
> 例えば、「メイリオ 英語名」や 「メイリオ 完全名 英語」などで調べてみてください。

`theme` オブジェクトでは、`text` ならグラフの文字全体、`plot_title` ではタイトルの文字と、部分的にフォントや大きさを調整することができます。
ここでは、タイトルのフォントだけをメイリオに変えてみます (Mac OS の人は `"meiryo"` を `"Hiragino Sans"` にしてください)。

```py
>>> from plotnine import ggplot, geom_point, aes, labs, theme, element_text
>>> (ggplot(data = mtcars, mapping = aes(x = "wt", y = "mpg")) 
... + geom_point() 
... + labs(title = "車両重量と燃費の散布図")
... + theme(plot_title = element_text(family = "meiryo")))
```

上手くいけば次のように表示できるはずです。

![](../../assets/class8/jp_font.png)

## グラフ出力

`ggplot` オブジェクトは [`save`](https://plotnine.readthedocs.io/en/stable/generated/plotnine.ggplot.html) メソッドで作成したグラフを出力できます。
基本的には `png` 形式をおすすめします。
また、`dpi` キーワードで解像度を上げておくと、拡大してもきれいに見えます。
デフォルトは 100 です。
300 ~ 500 ぐらいあれば十分だと思います。
ファイルパスの指定は今まで学んだことと同じです。

```py
>>> from plotnine import ggplot, geom_bar, aes
>>> from plotnine.data import mtcars
>>> g = ggplot(data = mtcars) + geom_bar(mapping = aes(x = "factor(am)")) # ggplot オブジェクト作成
>>> g.save("bar_plot.png") # グラフ出力
```