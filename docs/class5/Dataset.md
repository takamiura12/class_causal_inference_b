
# 雇用数増減データのダウンロード

[e-stat](https://www.e-stat.go.jp/) のサイトにアクセスします。
e-stat は様々な政府統計のデータをまとめているポータルサイトです。
「すべて」をクリックし、データベース一覧のページにアクセスします。

![](../../assets/class5/estatホームページ.png)

[データベース一覧](https://www.e-stat.go.jp/stat-search?page=1) から、*企業行動に関するアンケート調査* をクリックして、データの説明ページに移ります。

![](../../assets/class5/データベース一覧.png)


[データ内容](https://www.e-stat.go.jp/stat-search?page=1&toukei=00100402) のページのデータベースの欄をクリックして、データベース詳細のページに移ります。

![](../../assets/class5/データ内容.png)

[データベース詳細](https://www.e-stat.go.jp/stat-search/database?page=1&toukei=00100402&tstat=000001012479) で、上場企業か中堅・中小企業のデータどちらかを選びます。
今回は上場企業のデータを選択します。
「年度次」の部分をクリックして、データベース詳細のページに移ります。

![](../../assets/class5/データベース詳細.png)

[データセット一覧](https://www.e-stat.go.jp/stat-search/database?page=1&layout=datalist&toukei=00100402&tstat=000001012479&cycle=8&tclass1=000001116263&result_page=1&tclass2val=0) から、5-1 の産業別雇用者数の増減率 (過去 3 年間) の 「DB」というボタンをクリックして、統計表ページに移ります。

![](../../assets/class5/データセット一覧.png)

[統計表](https://www.e-stat.go.jp/dbview?sid=0003225718) から、ダウンロードというボタンをクリックすると、データダウンロードの設定ページが別ウインドウで開きます。

![](../../assets/class5/統計表ページ.png)

[ダウンロード設定](https://www.e-stat.go.jp/dbview/file-download?sid=0003225718) で、次のように設定して、ダウンロードボタンを押してデータをダウンロードします。

- ページ上部の選択項目 (年度次) を選択
  - 全データが欲しい場合は、全データを選択
- ファイル形式: 2 番目の、CSV 形式 (UTF-8, BOM あり) を選択
- ヘッダの出力: 出力しないを選択
- コードの出力: 出力しないを選択
- 階層コードの出力: 出力しないを選択
- 凡例の出力: 出力しないを選択
- 特殊文字の選択 (一番下の部分): Null に置き換えるを選択

![](../../assets/class5/ダウンロード設定.png)
