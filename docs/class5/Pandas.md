# Pandas 入門

[`pandas`](https://pandas.pydata.org/docs/index.html) は 様々な I/O (入出力) 処理、高機能なデータ処理の機能を提供する Python の外部ライブラリです。
例えば、`csv` や `xlsx` (エクセルのデータ形式) などのデータファイルの読み込み・書き込みや、複数のデータの結合などを可能にします。
まず、`pandas` の基本的な使い方について説明します。

## キーワード

- `Series`, `DataFrame`
- インデックス・カラム
- データ型
- I/O 処理
- `groupby`

## 基本的なデータ構造の概念

`pandas` で基本となるデータ構造 (クラス) は、`Series` と `DataFrame` です。
`Series` と `DataFrame` は、今までに学んだリストや辞書などのデータ構造と似ている部分もありますが、異なる部分もあります。
特に重要な概念について、先に触れておきます。

### インデックス・カラム

`Series` は 1 次元のラベル付きベクトル (または配列) で、`DataFrame` は 2 次元のラベル付き行列 (または表形式のデータ)です。
ラベルを用いることで特定の要素、`DataFrame` であれば特定の行や列のデータを参照できます。

**インデックス** は、データを横に切った場合の *行* を識別するためのラベルです。
`Series` と `DataFrame` どちらでも、インデックスによってデータの位置が特定できます。

**カラム** は、データを縦に切った場合の *列* を識別するためのラベルです。
`DataFrame` では、カラム毎に異なるデータ型を持つことができ、それぞれが特定の種類のデータを表します。

### データ型

**データ型** は、`DataFrame` のそれぞれのカラムのデータや `Series` のデータが数値、文字列、日付など何を表すかを定義します。
適切なデータ型を使用することが、データの分析・操作を効率的かつ正確に行うために重要です。

## Series, DataFrame の作成例

以下は `Series` の作成例です。

```py
>>> from pandas import Series
>>> Series([1, 2, 3])
0    1
1    2
2    3
dtype: int64
```

`0, 1, 2` というのが、各要素 (行) ごとについているラベル (インデックス) です。
このラベルを用いて、各要素 (行) を参照できます。

```py
>>> from pandas import Series
>>> data = Series([1, 2, 3])
>>> data[0]
1
```

以下は `DataFrame` の作成例です。

```py
>>> from pandas import DataFrame
>>> DataFrame({"item": ["apple", "melon"], "price": [100, 1000]})
    item  price
0  apple    100
1  melon   1000
```

`item`, `price` はカラムのラベルを示しています。
以下は、ラベルを用いて特定の列 (カラム) を参照しています。

```py
>>> from pandas import DataFrame
>>> data = DataFrame({"item": ["apple", "melon"], "price": [100, 1000]})
>>> data["item"]
0    apple
1    melon
Name: item, dtype: object
```

> [!note]
> R を使ったことがある人はピンとくるかもしれませんが、DataFrame は R の data.frame 型と同じようなものです。 実際、pandas は R のデータ整理用のパッケージ、dplyr でできることの多くを実装しています。 

## pandas のインポート

`pandas` のインポートは次のように行います。

```py
>>> import pandas
```

通常、`pandas` は `pd` という略称でインポートされることが多いです。 

```py
>>> import pandas as pd
```

> [!note]
> `pd` という略称は、`pandas` を使用する際の一般的な慣例です。
> これは多くの専門書やオンライン資料で一般的に見られます。
> 標準化された略称を使用することで、コードの可読性が向上し、他の開発者がコードを理解しやすくなります。

以降では、`pandas.<name>` を `pd.<name>` として表記します。

## データの読み込み

`pandas` は数多くのデータ形式の入出力処理に対応しています。

> [!tip]
> 入出力のことを **I/O** (input/output) と呼びます。

ここでは、よく使われる csv と xlsx のファイル読み込みを行います。
`read_xxx` という関数名が、特定のファイル形式の読み込みに対応しています。

- 読み込み用関数一覧: https://pandas.pydata.org/docs/reference/io.html

### CSV ファイルの読み込み

csv とは、comma separated values の略で、列ごとに値をカンマ `,` で区切って表します。
行ごとにデータが格納されています。
csv ファイルの読み込みには、`read_csv` 関数を用います。

読み込むデータファイルのパスは、絶対パスか相対パスで指定します。
REPL モードで Python を開いている場合はカレントディレクトリ、 `.py` スクリプトを `python <name>.py` で実行している場合は、`<name>` ファイルがあるディレクトリが起点となります。

> [!tip]
> データは `data` ディレクトリなどを作っておき、そこにまとめておくことをおすすめします。

例えば、[e-stat](https://www.e-stat.go.jp/) から取得した、2020 年度の産業別雇用数増減の csv データを読み込むとします。

メモ帳や VSCode で開くと、下の画像のように、値がカンマで区切られていることが分かります。
また、最初の行は列 (カラム) の名前を示しています。
データによってはカラム名がないこともあります。

![](../../assets/class5/CSVメモ帳.png)

> [!tip]
> Python で処理を行う前に、データファイルをメモ帳などで開いて、中身を確認しておくことをおすすめします。
> その際に、データを書き換えないように注意しましょう。

カレントディレクトリ内に `data` フォルダがあり、その中にこの csv ファイルがあると仮定します。
ファイル名は `japan_employee_change_2020.csv` です。
`read_csv` 関数内の引数として、ファイルのパスを指定します。
読み込んだデータを `DataFrame` に変換して返します。

```py
>>> import pandas as pd
>>> pd.read_csv("data/japan_employee_change_2020.csv")
      年度次       産業  /雇用者数の増減率 回答企業数【社】  △１５％以下【％】  △１５％超△１０％以下【％】  △１０％超△５％以下【％】  △５％超０％未満【％】  ０％【％】  ０％超５％未満【％】  ５％以上１０％未満【％】  １０％以上１５％未満【％】  １５％以上【％】  階級値平均【％】
0  2020年度      全産業        NaN    1,108        1.8             0.3            3.2         18.0   10.6        46.8          11.8            3.2       4.3      2.11
1  2020年度      製造業        NaN      498        0.8             0.2            2.8         18.5   12.4        49.6          10.2            2.2       3.2      1.95
2  2020年度   素材型製造業        NaN      148        1.4             NaN            2.0         16.2   11.5        53.4          10.1            2.0       3.4      2.09
3  2020年度   加工型製造業        NaN      203        0.5             0.5            2.5         20.2   10.3        50.7          10.3            3.0       2.0      1.88
4  2020年度  その他の製造業        NaN      147        0.7             NaN            4.1         18.4   16.3        44.2          10.2            1.4       4.8      1.89
5  2020年度     非製造業        NaN      610        2.6             0.3            3.4         17.5    9.2        44.6          13.1            3.9       5.2      2.25
```

### エクセル表データの読み込み

次に、エクセル形式の表データを読み込みます。
参考までに、先ほどの csv データをエクセルで開き、エクセルのデータ形式 (.xlsx) で保存します。
`read_excel` 関数を用いることで、読み込みが可能です。
エクセルのデータ形式のI/O処理には、`openpyxl` というライブラリを追加する必要があります。

```bash
pip instal openpyxl
```

インストール後に新しく Python を立ち上げて、実行します。

```py
>>> import pandas as pd
>>> pd.read_excel("data/japan_employee_change_2020.xlsx")
      年度次       産業  /雇用者数の増減率 回答企業数【社】  △１５％以下【％】  △１５％超△１０％以下【％】  △１０％超△５％以下【％】  △５％超０％未満【％】  ０％【％】  ０％超５％未満【％】  ５％以上１０％未満【％】  １０％以上１５％未満【％】  １５％以上【％】  階級値平均【％】
0  2020年度      全産業        NaN    1,108        1.8             0.3            3.2         18.0   10.6        46.8          11.8            3.2       4.3      2.11
1  2020年度      製造業        NaN      498        0.8             0.2            2.8         18.5   12.4        49.6          10.2            2.2       3.2      1.95
2  2020年度   素材型製造業        NaN      148        1.4             NaN            2.0         16.2   11.5        53.4          10.1            2.0       3.4      2.09
3  2020年度   加工型製造業        NaN      203        0.5             0.5            2.5         20.2   10.3        50.7          10.3            3.0       2.0      1.88
4  2020年度  その他の製造業        NaN      147        0.7             NaN            4.1         18.4   16.3        44.2          10.2            1.4       4.8      1.89
5  2020年度     非製造業        NaN      610        2.6             0.3            3.4         17.5    9.2        44.6          13.1            3.9       5.2      2.25
```

その他、読み込み用の関数には、文字コードなどの指定などができます。
詳しくは、関数のドキュメントを見てください。

- [`read_csv`](https://pandas.pydata.org/docs/reference/api/pandas.read_csv.html)
- [`read_excel`](https://pandas.pydata.org/docs/reference/api/pandas.read_excel.html)

## Series の基礎

### Series の作成

`Series` はリストやタプルなどのシーケンス型から生成できます。

```py
>>> from pandas import Series
>>> Series([1, 2, 3])
0    1
1    2
2    3
dtype: int64
```

リストやタプルなどと異なる部分があることがわかります。
順に説明します。

### インデックス

左の `0, 1, 2` がこの `Series` インスタンスのインデックスです。
リストやタプルなどでもインデックスがありましたが、それとは少し異なります。
`Series` や `DataFrame` におけるインデックスは、ある行を表す名前です。
何も指定しない場合、先ほどのように、`0, 1, 2` という通し番号が振られます。
リストやタプルにおけるインデックスは通し番号であったのに対し、`Series` や `DataFrame` のインデックスは指定・変更することが可能です。

インデックスをあらかじめ指定するには、クラスを作成するときの引数にインデックスのシーケンスを加えます。

```py
>>> from pandas import Series
>>> Series([1, 2, 3], index = ["A", "B", "C"])
A    1
B    2
C    3
dtype: int64
```

インデックスは `index` 属性として呼び出し可能で、また上書きをすることでインデックスを更新できます。

```py
>>> from pandas import Series
>>> data = Series([1, 2, 3])
>>> data.index
RangeIndex(start=0, stop=3, step=1)

>>> data.index = ["A", "B", "C"]
>>> data
A    1
B    2
C    3
dtype: int64
```

`RangeIndex(start=0, stop=3, step=1)` は `range` 関数が返すイテレーターのようなもので、0 から 3 の直前 (2) までの値を順に返すイテレーターであるということを意味しています。

データの長さとインデックスの長さ、つまり要素の数が一致していない場合はエラーになります。
以下のエラー文では、3 つの要素が求められているが、1 つしか得られなかったことでエラー (`ValueError`) になったと説明しています。

```py
>>> from pandas import Series
>>> data = Series([1, 2, 3])
>>> data.index = ["A"]
Traceback (most recent call last):
       ...
ValueError: Length mismatch: Expected axis has 3 elements, new values have 1 elements
```

> [!note]
> `Series` や `DataFrame` では、インデックスの値の重複を許します。
> 例えば、`Series([1, 2, 3], index = ["A", "A", "B"])` はエラーにはなりません。
> しかし、インデックスが重複すると思わぬエラーや意図せぬ挙動を引き起こす可能性があるため、インデックスはデータ内で一意にすることをおすすめします。

### Series の要素の取り出し

インデックスを指定することで、それに対応するデータの値を取り出すことができます。

```py
>>> from pandas import Series
>>> data = Series([1, 2, 3], index = ["A", "B", "C"])
>>> data["A"]
1
```

`loc` プロパティというものを使っても値を参照することができます。
プロパティは、インスタンスから値を参照したり、逆に値を追加したりするときに使うための機能を指します。
プロパティの呼び出しは、属性やメソッドと同じようにドット `.` を用います。
プロパティの使い方は、プロパティがどのような機能を持っているかによって異なってきます。

例えば、`loc` プロパティは次のように使います。

```py
>>> from pandas import Series
>>> data = Series([1, 2, 3], index = ["A", "B", "C"])
>>> data.loc["A"]
1
```

`loc("A")` ではないことに注意してください。

また、`iloc` プロパティというものもあり、これはリストやタプルと同じように、通し番号によって要素を参照できます。
Python ではお約束通り、通し番号は `0` から始まります。

`iloc` プロパティの使い方は `loc` プロパティと似ています。

```py
>>> from pandas import Series
>>> data = Series([1, 2, 3], index = ["A", "B", "C"]) # 通し番号は 0, 1, 2
>>> data.iloc[1] # 1 番目の要素を取り出す
2
```

また、負の値の通し番号も振られています。

```py
>>> from pandas import Series
>>> data = Series([1, 2, 3], index = ["A", "B", "C"]) # 負の値の通し番号は -3, -2, -1
>>> data.iloc[-1] # 最後の要素を取り出す
3
```

### データ型

`dtype` はこの `Series` のデータ型を示しています。
これは、中に含まれているデータ全体の型を表しています。
基本的にはデータの型を指定する必要はなく、`Series` を作成すると自動的にデータの全体の型を類推してくれます。

```py
>>> from pandas import Series
>>> Series([1, 2, 3])
0    1
1    2
2    3
dtype: int64
```

データは整数 (`int`) 型だったので、`dtype` は `int64` になりました。

> [!note]
> `int` の後の数字はビット数で、64 ビットは -9223372036854775808 から 9223372036854775807 までの整数値を扱えます。
> 他に、`int8` や `int16`、`int32` などがあります。
> 基本的には、ビット数が多いものを使えば良いでしょう。

小数点型をデータに含めるとどうなるでしょうか。

```py
>>> from pandas import Series
>>> Series([1.25, 2, 3])
0    1.25
1    2.00
2    3.00
dtype: float64
```

`dtype` は小数点のデータ型、`float64` になったことがわかります。

文字列をデータに加えてみます。

```py
>>> Series(["A", 1, 2])
0    A
1    1
2    2
dtype: object
```

`dtype` は `object` になりました。
文字列や、数値型などの特定の Python オブジェクトが入っている場合、また型がごちゃ混ぜになっているときはこのデータ型になります。

## DataFrame の基礎

### DataFrame の作成

`DataFrame` は値がリストの辞書やリストのリストなど、いくつかの方法を使ってデータを作成できます。
しかし、データの作り方によってどのような `DataFrame` ができるかが異なるので注意が必要です。

辞書の場合、各キーのリストが各カラムに対応します。

```py
>>> from pandas import DataFrame
>>> DataFrame({"item": ["apple", "melon"], "price": [100, 1000]}) 
    item  price
0  apple    100
1  melon   1000
```

リストが入れ子になっている場合、各リストのインデックスが各カラムに対応します。

```py
>>> from pandas import DataFrame
>>> data = DataFrame([["apple", 100], ["melon", 1000]]) 
       0     1
0  apple   100
1  melon  1000
```

### インデックス・カラム

`DataFrame` では各インデックスは行、カラムは列に対応しています。
`Series` と同様、インデックスを `DataFrame` 作成時に指定することが可能です。

```py
>>> from pandas import DataFrame
>>> DataFrame({"item": ["apple", "melon"], "price": [100, 1000]}, index = ["A", "B"]) 
    item  price
A  apple    100
B  melon   1000
```

または、`index` 属性を上書きします。

```py
>>> from pandas import DataFrame
>>> data = DataFrame({"item": ["apple", "melon"], "price": [100, 1000]}) 
>>> data.index = ["A", "B"]
>>> data
    item  price
A  apple    100
B  melon   1000
```

カラムも、インデックスと同様に、指定しない場合は自動で連番が振られます。
`columns` という引数名で指定可能です。
なお、辞書から作成した場合にはキーがカラム名として与えられます。

```py
>>> DataFrame([["apple", 100], ["melon", 1000]], columns = ["item", "price"])
    item  price
0  apple    100
1  melon   1000
```
`columns` 属性でカラム名を取得可能で、また上書きをすることもできます。

```py
>>> data = DataFrame({"item": ["apple", "melon"], "price": [100, 1000]}) 
>>> data.columns
Index(['item', 'price'], dtype='object')

>>> data.columns = ["I", "P"] # カラム名を上書き
>>> data
       I     P
0  apple   100
1  melon  1000
```

### DataFrame 内のデータの参照

`DataFrame` は `Series` よりもデータの次元が 1 つ多いため、データの取り出し方が少し複雑になります。
まず、特定のカラムを参照する場合には、鍵括弧 `[]` を使います。
`Series` の時ではこれは、インデックスでしたが、`DataFrame` ではカラムなのに注意してください。
また、列のデータが `Series` として返されることにも注意してください。

```py
>>> from pandas import DataFrame
>>> data = DataFrame({"item": ["apple", "melon"], "price": [100, 1000]}) 
>>> data["price"] # カラム名を指定する
0     100
1    1000
Name: price, dtype: int64
```

複数のカラムを指定する場合、鍵括弧内にカラム名の *リスト* を入れます。
リストを入れた場合には `DataFrame` が返ってきます。

```py
>>> data = DataFrame({
...     "item": ["apple", "melon"], 
...     "price": [100, 1000],
...     "sold": [10, 1],
...     }) 
>>> data
    item  price  sold
0  apple    100    10
1  melon   1000     1

>>> data[["price", "sold"]] # price と sold のカラムを指定する
   price  sold
0    100    10
1   1000     1
```

> [!note]
> うっかりリストを使い忘れた場合、タプル形式のカラム名を探します。
> 基本的に、これはカラム名が見つからないというエラー (`KeyError`) になります。

縦ではなく横、つまりあるインデックスに対応する行のデータを取り出したい場合には、`loc` プロパティもしくは `iloc` プロパティを使います。
`loc` プロパティと `iloc` プロパティの違いは、`Series` と同じで、ラベル名か連番かの違いです。

`loc` プロパティでインデックスを指定すると、それに対応する行の `Series` が返されます。

```py
>>> data = DataFrame({"item": ["apple", "melon"], "price": [100, 1000]}, index = ["A", "B"]) 
>>> data
    item  price
A  apple    100
B  melon   1000

>>> data.loc["A"] # インデックス A を指定
item     apple
price      100
Name: A, dtype: object
```

さらに、表でいうある行のある列の要素だけを取り出したい場合には、次のように `loc` プロパティでインデックスを指定してからカラム名を指定します。
対応する一つのオブジェクトが返ってきます。

```py
>>> data = DataFrame({"item": ["apple", "melon"], "price": [100, 1000]}, index = ["A", "B"])
>>> data
    item  price
A  apple    100
B  melon   1000

>>> data.loc["A", "item"] # インデックス A の カラム item の値
'apple'
```

### DataFrame のデータ型

`DataFrame` では、各データ型はカラムごとに定義されており、インスタンス作成時に自動でデータ型を類推してくれます。
また、各カラムのデータ型と、`DataFrame` 全体のデータ型は `dtypes` 属性 (複数形なのに注意!) で確認することができます。

```py
>>> data = DataFrame({"item": ["apple", "melon"], "price": [100, 1000]})
>>> data.dtypes
item     object
price     int64
dtype: object
```

## データの確認方法

### `head`, `tail`: データの一部を表示

データの行数、列数が多い場合、`DataFrame` や `Series` を確認すると、一部が省略された結果になります。
例えば、先ほどの産業別雇用数増減割合を 2001 年度から 2020 年度までで表したデータを読み込みます。
データの相対パスは `data/japan_employee_change.csv` とします。

```py
>>> pd.read_csv("data/japan_employee_change.csv")
        年度次       産業  /雇用者数の増減率 回答企業数【社】  △１５％以下【％】  △１５％超△１０％以下【％】  △１０％超△５％以下【％】  △５％超０％未満【％】  ０％【％】  ０％超５％未満【％】  ５％以上１０％未満【％】  １０％以上１５％未満【％】  １５％以上【％】  階級値平均【％】
0    2020年度      全産業        NaN    1,108        1.8             0.3            3.2         18.0   10.6        46.8          11.8            3.2       4.3      2.11
1    2020年度      製造業        NaN      498        0.8             0.2            2.8         18.5   12.4        49.6          10.2            2.2       3.2      1.95
2    2020年度   素材型製造業        NaN      148        1.4             NaN            2.0         16.2   11.5        53.4          10.1            2.0       3.4      2.09
3    2020年度   加工型製造業        NaN      203        0.5             0.5            2.5         20.2   10.3        50.7          10.3            3.0       2.0      1.88
4    2020年度  その他の製造業        NaN      147        0.7             NaN            4.1         18.4   16.3        44.2          10.2            1.4       4.8      1.89
..      ...      ...        ...      ...        ...             ...            ...          ...    ...         ...           ...            ...       ...       ...
115  2001年度      製造業        NaN      708        9.9             8.2           19.5         31.4    9.0        12.4           4.8            2.7       2.1     -3.62
116  2001年度   素材型製造業        NaN      196        9.7             8.7           20.9         35.7    8.7         8.7           3.6            3.1       1.0     -4.20
117  2001年度   加工型製造業        NaN      286        8.4             8.0           17.5         30.4    8.4        15.4           4.9            3.5       3.5     -2.74
118  2001年度  その他の製造業        NaN      226       11.9             8.0           20.8         28.8   10.2        11.9           5.8            1.3       1.3     -4.24
119  2001年度     非製造業        NaN      463        6.7             6.5           15.8         28.1    5.4        15.1          10.8            3.2       8.4     -0.80
```

データの先頭から指定行表示する場合、`head` メソッドを用います。
`head` メソッドは、デフォルトでは先頭 5 行を返します。
表示行数は引数で指定可能です。

```py
>>> data = pd.read_csv("data/japan_employee_change.csv")
>>> data.head()
      年度次       産業  /雇用者数の増減率 回答企業数【社】  △１５％以下【％】  △１５％超△１０％以下【％】  △１０％超△５％以下【％】  △５％超０％未満【％】  ０％【％】  ０％超５％未満【％】  ５％以上１０％未満【％】  １０％以上１５％未満【％】  １５％以上【％】  階級値平均【％】
0  2020年度      全産業        NaN    1,108        1.8             0.3            3.2         18.0   10.6        46.8          11.8            3.2       4.3      2.11
1  2020年度      製造業        NaN      498        0.8             0.2            2.8         18.5   12.4        49.6          10.2            2.2       3.2      1.95
2  2020年度   素材型製造業        NaN      148        1.4             NaN            2.0         16.2   11.5        53.4          10.1            2.0       3.4      2.09
3  2020年度   加工型製造業        NaN      203        0.5             0.5            2.5         20.2   10.3        50.7          10.3            3.0       2.0      1.88
4  2020年度  その他の製造業        NaN      147        0.7             NaN            4.1         18.4   16.3        44.2          10.2            1.4       4.8      1.89
```

逆に、データの最後から指定行前までを表示する場合、`tail` メソッドを用います。
`tail` メソッドもデフォルトで最後 5 行を返し、行数を引数で指定可能です。

```py
>>> data = pd.read_csv("data/japan_employee_change.csv")
>>> data.tail()
        年度次       産業  /雇用者数の増減率 回答企業数【社】  △１５％以下【％】  △１５％超△１０％以下【％】  △１０％超△５％以下【％】  △５％超０％未満【％】  ０％【％】  ０％超５％未満【％】  ５％以上１０％未満【％】  １０％以上１５％未満【％】  １５％以上【％】  階級値平均【％】
115  2001年度      製造業        NaN      708        9.9             8.2           19.5         31.4    9.0        12.4           4.8            2.7       2.1     -3.62
116  2001年度   素材型製造業        NaN      196        9.7             8.7           20.9         35.7    8.7         8.7           3.6            3.1       1.0     -4.20
117  2001年度   加工型製造業        NaN      286        8.4             8.0           17.5         30.4    8.4        15.4           4.9            3.5       3.5     -2.74
118  2001年度  その他の製造業        NaN      226       11.9             8.0           20.8         28.8   10.2        11.9           5.8            1.3       1.3     -4.24
119  2001年度     非製造業        NaN      463        6.7             6.5           15.8         28.1    5.4        15.1          10.8            3.2       8.4     -0.80
```

> [!tip]
> `pd.options.display.max_rows` を変更することで、省略なしに表示できる行数を変更することも可能です。
> デフォルトでは、これは 60 で、`pd.options.display.max_rows = 1000` とすれば、1000 行まで省略なしで表示されるようにできます。
> 他の `pandas` のオプションに関しては、[ドキュメント](https://pandas.pydata.org/docs/user_guide/options.html)を見てください。

### `info`: データの概要情報を取得

`info` メソッドを使うと、各カラムのデータ型 (Dtype) だけでなく欠損値以外の値の数 (Non-Null Count) やデータ容量 (memory usage) などの情報を得ることができます。

```py
>>> data = pd.read_csv("data/japan_employee_change.csv")
>>> data.info()
<class 'pandas.core.frame.DataFrame'>
RangeIndex: 120 entries, 0 to 119
Data columns (total 14 columns):
 #   Column          Non-Null Count  Dtype  
---  ------          --------------  -----  
 0   年度次             120 non-null    object 
 1   産業              120 non-null    object 
 2   /雇用者数の増減率       0 non-null      float64
 3   回答企業数【社】        120 non-null    object 
 4   △１５％以下【％】       117 non-null    float64
 5   △１５％超△１０％以下【％】  115 non-null    float64
 6   △１０％超△５％以下【％】   119 non-null    float64
 7   △５％超０％未満【％】     120 non-null    float64
 8   ０％【％】           120 non-null    float64
 9   ０％超５％未満【％】      120 non-null    float64
 10  ５％以上１０％未満【％】    119 non-null    float64
 11  １０％以上１５％未満【％】   118 non-null    float64
 12  １５％以上【％】        117 non-null    float64
 13  階級値平均【％】        120 non-null    float64
dtypes: float64(11), object(3)
memory usage: 13.2+ KB
```

### `describe`: 数値型データの記述統計量を取得

`describe` メソッドを使うと、各カラムの平均や標準偏差などの、記述統計量を確認することができます。

```py
>>> data.describe()
       /雇用者数の増減率   △１５％以下【％】  △１５％超△１０％以下【％】  △１０％超△５％以下【％】  △５％超０％未満【％】       ０％【％】  ０％超５％未満【％】  ５％以上１０％未満【％】  １０％以上１５％未満【％】    １５％以上【％】    階級値平均【％】
count        0.0  117.000000      115.000000     119.000000   120.000000  120.000000  120.000000    119.000000     118.000000  117.000000  120.000000
mean         NaN    3.710256        2.873913       7.759664    24.428333   10.650833   34.756667      9.464706       3.394915    3.459829    0.395333
std          NaN    3.202353        2.564231       5.987760     5.950696    3.128159   12.468661      3.531711       1.625694    1.988591    2.033510
min          NaN    0.500000        0.200000       0.800000    12.800000    4.900000    8.700000      2.700000       0.500000    0.500000   -5.000000
25%          NaN    1.300000        0.800000       3.150000    19.250000    8.400000   26.725000      7.400000       2.325000    1.900000   -0.210000
50%          NaN    2.700000        1.900000       5.600000    24.500000   10.300000   36.450000      9.200000       3.200000    3.200000    0.785000
75%          NaN    4.800000        3.750000       9.650000    28.875000   12.725000   44.000000     11.800000       4.200000    4.700000    1.912500
max          NaN   14.500000       10.600000      23.800000    38.900000   19.000000   58.800000     22.700000       8.700000    8.700000    3.190000
```

`describe` メソッドで記述統計量が表示されるのは、データ型が平均などを計算可能な数値に限られてしまうことに注意してください。

## データの操作

### フィルタリング

`Series` や `DataFrame` では、特定の列を参照するだけでなく、条件に合致したデータのみを取得する、フィルタリングを行うことが可能です。
単一の条件によるフィルタリングは次のように行います。

```py
data[data["column"] > value]
```

ここで、`data` は `DataFrame` のインスタンスで、`"column"` は条件を判別するときに用いるカラム名、`value` は条件を判別する値です。
このコードは、`data` の `<column>` というカラムのデータが `value` より大きい値のデータを返すという意味です。
条件式には、`>` (より大きい) だけでなく、`>=` (より大きいか等しい) や `==`、(等しい)、 `!=` (等しくない) などの演算子も使用可能です。

実際にやってみます。
例えば、以下のようなスーパーの商品の情報のデータがあるとします。

```py
>>> data = pd.DataFrame({
...     "product": ["Apple", "Banana", "Carrot", "Milk", "Beef"],
...     "price": [120, 200, 150, 200, 500],
...     "in_stock": [True, True, True, False, True],
...     "category": ["fruit", "fruit", "vegetable", "dairy", "meat"]
... })
>>> data
  product  price  in_stock   category
0   Apple    120      True      fruit
1  Banana    200      True      fruit
2  Carrot    150      True  vegetable
3    Milk    200     False      dairy
4    Beef    500      True       meat
```

このうち、価格が 150 円より高いものを選択するには次のようにします。

```py
>>> data[data["price"] > 150]
  product  price  in_stock category
1  Banana    200      True    fruit
3    Milk    200     False    dairy
4    Beef    500      True     meat
```

在庫がある商品データだけを取得したい場合は次のように行います。
在庫があれば `in_stock` は `True` で、なければ `False` です。

```py
>>> data[data["in_stock"] == True]
  product  price  in_stock   category
0   Apple    120      True      fruit
1  Banana    200      True      fruit
2  Carrot    150      True  vegetable
4    Beef    500      True       meat
```

`True` かどうかの条件式は省略可能です。

```py
>>> data[data["in_stock"]]
  product  price  in_stock   category
0   Apple    120      True      fruit
1  Banana    200      True      fruit
2  Carrot    150      True  vegetable
4    Beef    500      True       meat
```

> [!note|Question]
> 商品カテゴリーが果物のデータを選択してください。

また、複数の条件に合うものを選択することや、指定したカラムが特定の文字列を含むデータだけを選択するなど、より高度なフィルタリングも可能です。
この方法については、後ほど説明します。

## 新しい列の追加と削除

`DataFrame` には、新しいデータ列を追加したり、不要な列を削除したりする機能があります。
これにより、データの加工や整形が容易になります。

### 新しい列の追加

新しい列を追加するには、以下のように列名を指定して値を代入します。

```py
data['new_column'] = value
```

この `value` には、`Series`、リスト、タプル、または単一の値を使用することができます。

例えば、商品ごとに消費税を含んだ価格を計算して、新しい列として追加します。
消費税率を 10% と仮定して、以下のようにします。

```py
>>> data = pd.DataFrame({
...     "product": ["Apple", "Banana", "Carrot", "Milk", "Beef"],
...     "price": [120, 200, 150, 200, 500],
...     "in_stock": [True, True, True, False, True],
...     "category": ["fruit", "fruit", "vegetable", "dairy", "meat"]
... })
>>> data['price_with_tax'] = data['price'] * 1.1 # price の各値を 1.1 倍したものを追加
>>> data
  product  price  in_stock   category  price_with_tax
0   Apple    120      True      fruit           132.0
1  Banana    200      True      fruit           220.0
2  Carrot    150      True  vegetable           165.0
3    Milk    200     False      dairy           220.0
4    Beef    500      True       meat           550.0
```

`data["price"]` は各商品の価格を表す `Series` です。
この `Series` の各要素に対して 1.1（消費税 10% ）を掛けています。
このような四則演算は、`pandas` においては要素ごと（element-wise）に行われ、各行の同じ位置にある要素同士の演算となります。

単一の値を新しい列に追加する例も見てみましょう。
すべての商品に「日本」という原産国を追加するには、以下のようにします。

```py
>>> data['origin'] = 'Japan'
>>> data
  product  price  in_stock   category  price_with_tax origin
0   Apple    120      True      fruit           132.0  Japan
1  Banana    200      True      fruit           220.0  Japan
2  Carrot    150      True  vegetable           165.0  Japan
3    Milk    200     False      dairy           220.0  Japan
4    Beef    500      True       meat           550.0  Japan
```

### 列の削除

列を削除するには、`drop` メソッドを使用し、`columns` キーワードで削除したい列名を指定します。

```py
data.drop(columns='column_name')
```

例えば、`in_stock` 列を削除するには、以下のようにします。

```py
>>> data.drop(columns='in_stock')
  product  price   category  price_with_tax origin
0   Apple    120      fruit           132.0  Japan
1  Banana    200      fruit           220.0  Japan
2  Carrot    150  vegetable           165.0  Japan
3    Milk    200      dairy           220.0  Japan
4    Beef    500       meat           550.0  Japan
```

> [!note]
> `drop` メソッドはデフォルトで新しい `DataFrame` を返しますが、元の `DataFrame` は変更しません。
> 元のデータに変更を適用する場合は、`inplace = True` を引数に追加します。

### 行の削除

特定の行を削除するには、行のインデックスを指定します。
列を指定するときとは異なり、`index` キーワードで指定するか、`axis = 0` を使用するか、または指定を省略します。

```py
data.drop("index_name")
```

例えば、インデックス `2` の行（Carrot のデータ）を削除するには、以下のようにします。

```py
>>> data.drop(2)
  product  price  in_stock   category  price_with_tax origin
0   Apple    120      True      fruit           132.0  Japan
1  Banana    200      True      fruit           220.0  Japan
3    Milk    200     False      dairy           220.0  Japan
4    Beef    500      True       meat           550.0  Japan
```

> [!tip]
> `axis` キーワードを使用することで、列 (`axis=1`) または行 (`axis=0`) を指定して削除することも可能です。
> しかし、`columns` と `index` キーワードを使用する方が直感的であり、特に初学者には推奨される方法です。

これらの操作を活用することで、データの整形や加工を容易に行うことができます。

第1回目の講義資料の最後のセクション、「基本的なデータの集計」の叩き台を作成しました。

## 基本的なデータの集計

データの集計は、データ分析において重要なステップの一つです。
`pandas` では、データをグループ化し、集計メソッドを適用することで、データセットの要約や分析を行うことができます。

### 集計メソッドの使用

`pandas` には、データを集計するためのいくつかのメソッドが用意されています。
これらのメソッドを使用することで、合計、平均、最大値、最小値などの計算が可能です。

- `.sum()`: 合計を計算
- `.mean()`: 平均を計算
- `.max()`: 最大値を計算
- `.min()`: 最小値を計算

これらのメソッドは、`DataFrame` の列に直接適用することも、`groupby` メソッドを使って特定のカテゴリごとに適用することもできます。

例として、先ほどのスーパーの商品データを使用して、全体の価格の平均を計算します。
集計できるのは、数値型のデータ列のみであることに注意してください。

```py
>>> data = pd.DataFrame({
...      "product": ["Apple", "Banana", "Carrot", "Milk", "Beef"],
...      "price": [120, 200, 150, 200, 500],
...      "category": ["fruit", "fruit", "vegetable", "dairy", "meat"],
...  })
>>> data["price"].mean() # price 列の平均値を計算
234.0
```

### グループ別集計の例

カテゴリごとに価格の平均を計算してみましょう。
カテゴリごとの集計を行うには、まず `groupby` メソッドを使います。

```py
data.groupby("category_name")
```

その後に、集計を行う列を指定し、集計メソッドを適用します。
例えば、商品カテゴリごとに価格の平均を計算するには、`groupby` メソッドで `category` 列を指定し、その後に `price` 列を指定し、`mean` メソッドを適用します。

```py
>>> data.groupby('category')['price'].mean()
category
dairy        200
fruit        160
meat         500
vegetable    150
Name: price, dtype: float64
```

この結果から、各カテゴリに属する商品の平均価格がわかります。
例えば、フルーツの平均価格は 160 円、肉類の平均価格は 500 円です。

このように、`pandas` の集計関数を使用することで、データセットの要約や傾向の理解が可能になります。
