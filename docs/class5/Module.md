
# モジュール・パッケージ・ライブラリ

Python の基本的な機能だけでも簡単な計算や分析などが可能ですが、それだけでは足りないこともあります。
ここでは、そういった追加機能を使うための方法や、追加機能を外部からインストールするための方法を学びます。

## キーワード
- `import`
- モジュール
- `pip`
- パッケージ・ライブラリ

## モジュールの追加

### モジュールとは

Python を立ち上げたときに使える関数 (`len` 関数など) やクラス (`int` 型など) はそれぞれ組み込み関数・クラス、または **ビルトイン** (built-in) 関数・クラスと呼ばれます。
しかし、それだけでは機能が不十分です。
そのような場合、**モジュール** (module) を追加します。
モジュールとは、クラスや関数などの集まりを指します。
後にわかるように、モジュールは単なる `py` スクリプトです。

### `import` 構文

時間のデータを扱いたいとします。
`2022/12/24` というデータから、年 (`2022`) だけを参照したり、ある日時との差を計算したりといった機能が欲しいです。
このような場合、`str` 型では不十分であり、当然 `int` 型などの数値型でも扱いが難しいです。
Python には `datetime` モジュールという、時間を扱うクラス・関数が用意されています。
モジュールを追加するには、`import` 構文を使用します。

`<module_name>` を追加するには次のように書きます。

```py
>>> import <module_name>
```

モジュールが正しく追加された場合は、何も出力されません。
下のコードは `datetime` モジュールを追加しています。

```py
>>> import datetime
>>>
```

モジュールが見つからない場合には、エラー (`ModuleNotFoundError`) が返ってきます。

```py
>>> import wrong_module
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ModuleNotFoundError: No module named 'wrong_module'
```

### モジュールの扱い

モジュールを追加すると、以降はそのモジュールが使用可能です。
REPL モードの場合、起動している Python を閉じるまではそのモジュールが使用できます。
これは、変数や関数などを定義すれば、その後に定義した変数・関数が使用できることと同じです。

追加した `datetime` モジュールを参照してみます。

```py
>>> datetime
<module 'datetime' from '/usr/lib/python3.8/datetime.py'>
```

`datetime` モジュールが `datetime.py` から追加されていると書いてあります。
モジュールは `py` スクリプト内に定義されているクラス・関数・定数などを追加しているのです。
`datetime.py` 内のクラスなどは `datetime` という名前にまとめられています (**名前空間** と呼びます)。

モジュール内のクラスなどを使うには、クラスの属性を呼び出すときのようにドット `.` を用いて、 `<module_name>.<obj>` とします。

例えば、`datetime` モジュール内には、時間を扱う `datetime` クラスや日にちを扱う `date` クラスなどがあります。
以下のコードは、これらを `datetime` モジュールから参照しています。

```py
>>> datetime.datetime # datetime クラスの呼び出し
<class 'datetime.datetime'>

>>> datetime.date # date クラスの呼び出し
<class 'datetime.date'>
```

### `from ~ import` 構文

モジュール内の一部だけを追加したい場合、例えば `datetime` モジュールの `datetime` クラスや `date` クラスだけを追加したい場合は、`from ~ import` 構文を使います。
`<moduel_name>` から `<obj>` を追加したい場合、次のように書きます。

```py
from <module_name> import <obj>
```

以下は `datetime` モジュールから `date` クラスを追加しています。
`import` で指定した名前がそのまま使えるようになっていることに注意してください。
また、`from <module_name>` で指定したモジュールは、他で `import` を行っていない場合には使えないことにも注意してください。
例えば、以下のコードは新しく Python を立ち上げて実行した結果です。

```py
>>> datetime # datetime を追加していないのでエラー
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'datetime' is not defined

>>> date # date クラスを追加していないのでエラー
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'date' is not defined

>>> from datetime import date # date クラスを datetime モジュールから追加
>>> date # 追加したので使える
<class 'datetime.date'>

>>> datetime # datetime は追加していないままなのでエラー
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'datetime' is not defined
```

また、モジュールから複数のオブジェクトを追加することも可能です。
オブジェクト間は `,` で区切ります。
次は `datetime` から `datetime` クラスと `date` クラスを追加しています。

```py
>>> from datetime import datetime, date
```

### `import ~ as` 構文

追加モジュールやモジュール内の一部のオブジェクト名が長い場合、`import ~ as` 構文を使って、名前を変更することができます。

以下は、`datetime` モジュールを `dt` というモジュール名として追加しています。
新しく立ち上げた Python で以下を実行しています。

```py
>>> import datetime as dt # datetime モジュールを dt として追加
>>> dt # datetime モジュールが参照される
<module 'datetime' from '/usr/lib/python3.8/datetime.py'>

>>> datetime # datetime という名前では追加していないのでエラー
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'datetime' is not defined
```

`from ~ import` を組み合わせることも可能です。
名前を変更する場合、各オブジェクト毎に行うことに注意してください。
次のコードは、`datetime` モジュールから `datetime` クラスを `dt` という名前で追加し、`date` クラスはそのままの名前で追加しています。

```py
>>> from datetime import datetime as dt, date # datetime クラスを dt として追加、date はそのまま
>>> dt # datetime クラスが参照される
<class 'datetime.datetime'>

>>> date # date クラスはそのままの名前
<class 'datetime.date'>
```

### `datetime`, `date` クラスを使ってみる

`datetime` は秒などの細かい単位までの時間を扱えるクラスで、`date` はそれより粗く日にち単位までの時間を扱うクラスです。

`datetime` クラスは次のような年 (year)、月 (month)、日 (day)、時 (hour)、分 (minute)、秒 (second)、マイクロ秒 (microsecond) まで指定可能です。
日にちより細かい時間は省略可能です。
例えば、2024 年 4 月 1 日 の `datetime` 型は次のように作成します。

```py
>>> datetime(2024, 4, 1)
datetime.datetime(2024, 4, 1, 0, 0)
```

年や月などは属性として用意されています。

```py
>>> day = datetime(2024, 4, 1)
>>> day.year
2024

>>> day.month
4
```

`date` は日付まで指定可能です。

```py
>>> date(2024, 4, 1)
datetime.date(2024, 4, 1)
```

その他、`timedelta` クラスを使うことで、ある時間から一定期間後の時間はいつになるかを計算できます。
また、文字列から `datetime`, `date` 型に変換することや、その逆も可能です。

詳しくは Python のドキュメントを見てください。
https://docs.python.org/ja/3/library/datetime.html#datetime.datetime


# パッケージ・ライブラリのインストール

複数のファイルをディレクトリにまとめられるように、モジュールもディレクトリにまとめることができます。
モジュールのあつまりを **パッケージ** (package) と呼び、さらにパッケージがあつまったものを **ライブラリ** (library) と呼びます。

> [!note]
> 実際には、パッケージとライブラリは同じような意味で使われることが多いです。
> また、パッケージをモジュールと呼んでいる場合もあります。

Python をインストールしたときに用意されている機能だけでなく、拡張した機能が外部で公開されています。
まずは、データ操作でよく使われる [`pandas`](https://pandas.pydata.org/) というライブラリをインストールしましょう。
インストールには、`pip` コマンドを使います。

> [!note]
> `pip` コマンドは `python` を立ち上げていない状態で実行します。

`pip` コマンドでライブラリをインストールする場合は、次のように入力します。

```bash
pip install <name>
```

`pandas` をインストールするには次のコマンドです。
正しく入力されていれば、`pandas` と `pandas` 内で使っている他の外部ライブラリが追加されます。

> [!note]
> 必ずしもライブラリ名と `pip install` のときに指定する名前が一致するわけではありません。
> まずは google 検索などでインストールするときの名前を調べましょう。
> 
```bash
pip install pandas
```

Python を新しく立ち上げて、`pandas` を `import` で追加できるようになっていることを確認します。

```bash
>>> import pandas
```
 
> [!tip]
> 特に初心者の場合、どのようなライブラリがあるのかを把握するのが難しいと思います。
> 
> その場合は以下のことをおすすめします。
> 1. Python の人気ライブラリ・おすすめライブラリなどをまとめているブログ記事を読む
> 2. ChatGPT などで聞いてみる。例えば、「Python で大規模なデータ分析を行うにはどのようなライブラリがありますか。」など。
> 3. [Qiita](https://qiita.com/) や [Medium](https://medium.com/) などで Python 用の記事を読む
