# 小さいデータの処理

## キーワード

- パース

## 課題設定

リストを使って、以前行った BMI 計算などを含む健康診断レポートプログラムを作成します。
今回は、10 人のデータを扱います。
健康診断のデータには個人ID, 身長, 体重, 年齢, 血圧のデータが記載されています。

`small_data.py` というファイルを作成して、入力データを定義します。
(実装例を `src/class4/small_data.py` に格納しています)

```py
DATA = [
    "A,167,71,58,135",
    "B,160,79,53,111",
    "C,178,83,34,133",
    "D,173,66,20,129",
    "E,176,82,57,122",
    "F,167,85,44,94",
    "G,160,80,51,106",
    "H,151,82,44,132",
    "I,153,65,41,108",
    "J,169,58,56,91"
]
```

上のようなデータは、`csv` ファイルを読み込んだ場合に発生します。
やることは以前と変わらず、BMI と肥満度を計算します。

個人毎のデータに対して、個人ID, BMI, 肥満度の辞書型を作成して、そのリストを作成します。
つまり、次のような結果になることを期待しています。

```py
[
    {'ID': 'A', 'BMI': 25.46, 'Obesity': 'Overweight'},
    {'ID': 'B', 'BMI': 30.86, 'Obesity': 'Overweight'},
    {'ID': 'C', 'BMI': 26.2, 'Obesity': 'Overweight'},
    {'ID': 'D', 'BMI': 22.05, 'Obesity': 'Normal'},
    {'ID': 'E', 'BMI': 26.47, 'Obesity': 'Overweight'},
    {'ID': 'F', 'BMI': 30.48, 'Obesity': 'Overweight'},
    {'ID': 'G', 'BMI': 31.25, 'Obesity': 'Overweight'},
    {'ID': 'H', 'BMI': 35.96, 'Obesity': 'Overweight'},
    {'ID': 'I', 'BMI': 27.77, 'Obesity': 'Overweight'},
    {'ID': 'J', 'BMI': 20.31, 'Obesity': 'Normal'}
]
```

入力データから出力データを作成するのに、どのようなステップが必要か考えてみましょう。

  1. リストから要素を取り出す
  2. 文字列からデータを抽出する
  3. 身長・体重から BMI を計算する
  4. BMI から肥満度のカテゴリー分けをする
  5. 個人ID, BMI, 肥満度の辞書型を作成する
  6. 処理を繰り返す
  7. 結果をリストにまとめる

やることリスト `small_data.py` に追加します。

```py 

# 1. リストから要素を取り出す
# 2. 文字列からデータを抽出する
# 3. 身長・体重から BMI を計算する
# 4. BMI から肥満度のカテゴリー分けをする
# 5. 個人ID, BMI, 肥満度の辞書型を作成する
# 6. 処理を繰り返す
# 7. 結果をリストにまとめる

```

プログラムの実装は、必ずしもやることリストの上から順に行う必要はありません。
やることを大まかなカテゴリーに分けると、2 ~ 5 はデータの各要素に対する操作で、1, 6, 7 はそれ以外に分けられます。
どちらを先に実装するかに正解はありませんが、ここでは 2 ~ 5 を先に行います。

まずは例となる文字列を取得します。
データから適当な要素を取得して、変数に格納します。

```py

DATA = [
    ...
]

# 2. 文字列からデータを抽出する
TEXT = DATA[0]
print(TEXT)
```

実行すると次の結果を得ます。

```bash
python small_data.py
A,167,71,58,135
```

> [!tip]
> `print` は、改行コード `\n` などがあると、改行した結果が表示されます。
> 例: `print("A\nB")`
> 改行コードもそのまま出力したいときは、`repr` 関数を通してから `print` で出力します。 
> 例: `print(repr("A\nB"))`

## 文字列の解析

テキストを解析し、データなどの情報を抽出することを **パース** (parse) といいます。
まずは、個人ID, 身長などのデータが 1 つの文字列になっているので、これを小分けにします。
`split` メソッドを用いることで、区切り文字に従って文字列をリストに分割できます。
デフォルトの区切り文字は空白文字 (スペース、改行コード `\n` やタブ `\t`) です。
ここではカンマ `,` を区切り文字として用います。 

`small_data.py` に追加して、リストに分割します。

```py
# 2. 文字列からデータを抽出する
TEXT = DATA[0]
print(TEXT) # 入力データの一部を出力

TEXT_DATA = TEXT.split(",") # 文字列をリストに分割
print(TEXT_DATA)
```

実行すると次の結果を得ます。

```bash
python small_data.py
'A,167,71,58,135'
['A', '167', '71', '58', '135']
```

### BMI の計算

リストの 1 番目が身長 (cm) で 2 番目が体重 (kg) です。
ここから、BMI を計算します。
次のような関数を作成します。

```py
# 3. 身長・体重から BMI を計算する
def calc_BMI(data):
    height = data[1]
    weight = data[2]
    bmi = weight / (height / 100) ** 2
    return bmi

BMI = calc_BMI(TEXT_DATA)
print(BMI) 
```


しかし、このままだと文字列を文字列のリストにしただけなので、エラーになります。
整数の文字列を整数型に変換するなら `int` 関数, 小数点型にするなら `float` 関数を用います。
ここでは `int` 関数を使います。

```py
def calc_BMI(data):
    height = int(data[1])
    weight = int(data[2])
    bmi = weight / (height / 100) ** 2
    return bmi

BMI = calc_BMI(TEXT_DATA) 
print(BMI) # BMI を出力
```

実行すると次の結果を得ます。

```bash
python small_data.py
A,167,71,58,135
['A', '167', '71', '58', '135']
25.458065904119906
```

`round` 関数を用いて、小数点 2 桁まで丸めます。
`round(<obj>, i)` とすると、小数点オブジェクト `<obj>` を `i` 桁まで丸めた結果を返します。


```py
def calc_BMI(data):
    height = int(data[1]) # 文字列型を整数型に変換
    weight = int(data[2])
    bmi = weight / (height / 100) ** 2
    return round(bmi, 2) # 小数点 2 桁丸めた結果を返す

BMI = calc_BMI(TEXT_DATA) 
print(BMI) # BMI を出力
```

実行すると次の結果を得ます。

```bash
python small_data.py
A,167,71,58,135
['A', '167', '71', '58', '135']
25.46
```

### 肥満度カテゴリー分け

以前作成したカテゴリー分けの関数を用います。

```py
# 4. BMI から肥満度のカテゴリー分けをする
def classify_BMI(bmi):
    if bmi > 25:
        return "Overweight"

    elif bmi < 18.5:
        return "Underweight"
    
    else:
        return "Normal"

OBESITY = classify_BMI(BMI)
print(OBESITY) # 肥満度カテゴリーを出力
```

実行すると次の結果を得ます。

```bash
python small_data.py
A,167,71,58,135
['A', '167', '71', '58', '135']
25.46
Overweight
```

### 診断レポート作成

最後に、必要なデータを辞書型にまとめます。

```py
# 5. 個人ID, BMI, 肥満度の辞書型を作成する
USER_ID = TEXT_DATA[0]
REPORT = {
    "ID": USER_ID,
    "BMI": BMI,
    "Obesity": OBESITY
}
print(REPORT) # 診断レポートを出力
```

実行すると次の結果を得ます。

```bash
pytohn small_data.py
A,167,71,58,135
['A', '167', '71', '58', '135']
25.46
Overweight
{'ID': 'A', 'BMI': 25.46, 'Obesity': 'Overweight'}
```

文字列から診断レポートまでの流れをひとまとめにします。
つまり、文字列を入力すると診断レポートを表す辞書型を返す関数を作成します。
`calc_BMI`, `classify_BMI` はそのままに、細々とした変数定義などを関数内で行います。

```py
def calc_BMI(data):
    height = int(data[1]) # 文字列型を整数型に変換
    weight = int(data[2])
    bmi = weight / (height / 100) ** 2
    return round(bmi, 2) # 小数点 2 桁丸めた結果を返す

def classify_BMI(bmi):
    if bmi > 25:
        return "Overweight"

    elif bmi < 18.5:
        return "Underweight"
    
    else:
        return "Normal"

def make_report(data_str):
    data = data_str.split(",") # 文字列をリストに分割

    # 身長・体重から BMI を計算する
    bmi = calc_BMI(data) 

    # BMI から肥満度のカテゴリー分けをする
    obesity = classify_BMI(bmi)

    # 個人ID, BMI, 肥満度の辞書型を作成する
    user_id = data[0]
    report = {
        "ID": user_id,
        "BMI": bmi,
        "Obesity": obesity
    }

    # 診断レポートを返す
    return report

# 入力データの一部を出力
TEXT = DATA[0]
print(TEXT)
print(make_report(TEXT))
```

実行すると次の結果を得ます。

```bash
python small_data.py
A,167,71,58,135
{'ID': 'A', 'BMI': 25.46, 'Obesity': 'Overweight'}
```

### ループ処理

入力データの各要素に対して、`make_report` 関数を使い、診断レポートを作成します。
リストのループ処理なので、`for` ループを使います。
先ほどの入力データの一部を出力するコードは削除しても構いません。

```py
# 6. 処理を繰り返す
for data_str in DATA:
    report = make_report(data_str)
    print(report)
```

実行すると次の結果を得ます。

```bash
python small_data.py
{'ID': 'A', 'BMI': 25.46, 'Obesity': 'Overweight'}
{'ID': 'B', 'BMI': 30.86, 'Obesity': 'Overweight'}
{'ID': 'C', 'BMI': 26.2, 'Obesity': 'Overweight'}
{'ID': 'D', 'BMI': 22.05, 'Obesity': 'Normal'}
{'ID': 'E', 'BMI': 26.47, 'Obesity': 'Overweight'}
{'ID': 'F', 'BMI': 30.48, 'Obesity': 'Overweight'}
{'ID': 'G', 'BMI': 31.25, 'Obesity': 'Overweight'}
{'ID': 'H', 'BMI': 35.96, 'Obesity': 'Overweight'}
{'ID': 'I', 'BMI': 27.77, 'Obesity': 'Overweight'}
{'ID': 'J', 'BMI': 20.31, 'Obesity': 'Normal'}
```

### 結果をリストにまとめる

最後に、これらの各診断レポートを一つのリストにまとめます。
まずは空のリストを作成し、`append` で追加していきます。

```py
# 6. 処理を繰り返す
# 7. 結果をリストにまとめる
results = []
for data_str in DATA:
    report = make_report(data_str)
    results.append(report) # 診断レポートを追加

print(results[:3]) # 結果の一部を出力
```

実行すると次の結果を得ます。

```bash
python small_data.py
[{'ID': 'A', 'BMI': 25.46, 'Obesity': 'Overweight'}, {'ID': 'B', 'BMI': 30.86, 'Obesity': 'Overweight'}, {'ID': 'C', 'BMI': 26.2, 'Obesity': 'Overweight'}]
```

### 補足: `HealthReport` クラスを作る

データの文字列から、解析、BMI 計算、肥満度カテゴリー分け、データ出力を `HealthReport` クラスにまとめてみます。
インスタンスを作成する際にデータの文字列を渡し、`parse_text` メソッドで文字列をリストに分割し、結果をインスタンス属性 `data` に格納します。
`make` メソッドで結果を出力します。
(実装例を `src/class4/small_data_class.py` に格納しています)

```py
class HealthReport:

    def __init__(self, data_str):
        self.data = self.parse_text(data_str) # 文字列をリストに分解

    def parse_text(self, data_str):
        return data_str.split(",")

    def get_user_id(self):
        return self.data[0]

    # 身長・体重から BMI を計算する
    def calc_BMI(self):
        height = int(self.data[1]) # 文字列型を整数型に変換
        weight = int(self.data[2])
        bmi = weight / (height / 100) ** 2
        return round(bmi, 2) # 小数点 2 桁丸めた結果を返す

    # BMI から肥満度のカテゴリー分けをする
    def classify_BMI(self):
        bmi = self.calc_BMI()
        if bmi > 25:
            return "Overweight"

        elif bmi < 18.5:
            return "Underweight"
        
        else:
            return "Normal"


    # 個人ID, BMI, 肥満度の辞書型を出力する
    def make(self):
        return {
            "ID": self.get_user_id(),
            "BMI": self.calc_BMI(),
            "Obesity": self.classify_BMI(),
        }

results = []
for data_str in DATA:
    report = HealthReport(data_str).make() # 診断レポートを出力
    results.append(report) # 診断レポートを追加

print(results[:3]) # 結果の一部を出力
```