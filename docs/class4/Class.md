
# クラス

ここでは、新しいオブジェクトのクラス (型) を作成する方法について学んでいきます。
少し長くなるので、`class_example.py` という py スクリプトを作成します。
ここは初学者にとって難しいので、完全に理解できなくても大丈夫です。
しかし、ある程度理解しておくことで、よりプログラミングが効率的になります。

## キーワード

- `class`
- インスタンス
- クラス属性・インスタンス属性
- メソッド
- 初期化メソッド

## なぜクラスが必要か

処理が複雑化すると、関数や変数が多くなりごちゃついてきます。
例えば、健康診断のデータから健康診断レポートを作成するプログラムを考えます。

データには身長、体重など多くの情報が入ってるとします。
前に作成した BMI の計算関数や BMI を元にした肥満度カテゴリー分けだけでなく、血圧が正常範囲かの判定など、色々な機能が考えられます。

```py

def calc_BMI(height, weight):
    ...

def classify_obesity(BMI):
    ...

def classify_BP(bp): # BP は blood pressure の略
    ...

def make_report(data):

    bmi = calc_BMI(data["height"], data["weight"])
    obesity = classify_obsesity(bmi)
    bp_category = classify_BP(data["BP"])

    return {
        "BMI": bmi,
        "Obesity": obesity
        "BP_category": bp_category,
    }
```

最後の関数 `make_report` はこれらの健康診断レポートに必要な操作をまとめています。
肥満度カテゴリー分けの前には BMI の計算が必要なので、先に実行する必要があります。
しかし、この方法にはいくつか問題が起こりえます。

1. 関数にまとまりがない
   健康診断レポートに必要な関数だけでなく、他の関数も作成した場合、どれが何に必要な関数か分かりにくくなります
   
2. 関数が肥大化する
   `make_report` 内で条件分岐などを行ったりすると、関数が長くなりバグが起こりやすくなります。

## `class` 構文

関数ではなく新しいクラス (型) を定義することで、データと必要な操作 (関数) をまとめることができます。
新しいオブジェクトのクラスを作成するには、`class` 構文を使用します。
構文は次のような規則です。

```py
class <ClassName>:
        
    <name> = ...

    def <method>:
        ...
```

作成したクラスは `<ClassName>` というクラス名として定義されます。

### 属性の定義

関数の定義などと同様、インデントブロック内にクラスの属性・メソッドを定義します。
上のコードは `<name>` という属性と `<method>` というメソッドを定義しています。

例えば、次は `Calender` クラスというものを作成し、`year` という属性を定義し、そこに `2023` という値を格納しています。
`class_example.py` に追記します。

```py
class Calender:
    
    year = 2023 # 属性の定義

print(Calender.year) # 属性の参照
```

実行すると次を返します。

```bash
python class_example.py
2023
```

> [!note|label:Question]
> `HealthReport` クラスを作成して、`height`, `weight` という属性を定義してみましょう。

### メソッドの定義

メソッドでは、オブジェクトが保持しているデータを使うことができます。
これは関数との大きな違いです。
しかし、その際に内部のデータをアクセスするためのオブジェクト自身の名前を表す必要があります。
Python では伝統的にオブジェクト自身を `self` という変数名で表します。

> [!tip]
> JavaScript などでは `this` という名前を使用します。
> 変数名は実際には何でもよいですが、他の人も分かりやすいように合わせた方が無難です。

メソッドでは、最初の引数がオブジェクト自身を表す変数名で、2 番目以降が追加の引数です。
例えば、現在の年を出力する `print_current_year` メソッドと、指定した年数分先を返す `print_future_year` メソッドを定義します。

```py
class Calender:

    year = 2023 # 属性の定義

    def print_current_year(self): # メソッドの定義
        print(self.year) # オブジェクト内の `year` を参照

    def print_future_year(self, year_length):
        print(self.year + year_length)

calender = Calender() # インスタンス作成
print(calender.year) # 属性呼び出し
print(calender.print_current_year()) # メソッド呼び出し
print(calender.print_current_year(10)) 
```

実行すると次を返します。

```bash
python class_example.py
2023
2023
2033
```

`self.<name>` とすることで、オブジェクト内の `<name>` という属性を呼び出せます。
また、メソッドを呼び出すことも可能です。

`def` 構文で定義した関数とは異なり、最初の `self` は引数として省略されます。
したがって、`print_current_year` は引数は 1 つなので、このメソッドの呼び出し時に引数を入れる必要はありません。
`print_future_year` は引数が 2 つなので、`year_length` の引数をメソッドを呼び出すときに加える必要がありません。

しかし、さきほどの `class` 内で定義した属性の呼び出しの前に、`calender = Calender()` というコードが書かれています。
`class` で定義したクラスは、いわば新しいクラスの設計図のようなもので、カレンダーの機能 (メソッド) を使うには、まず設計図からモノ (オブジェクト) を作成する必要があります。
クラスから生成したオブジェクトを、**インスタンス** (instance) と呼びます。

> [!note|label:Question]
> `HealthReport` クラスに `weight`, `height` の属性を使って、BMI を計算するメソッドを作成しましょう。

### インスタンス属性

先ほどの例では、`year` が固定されているのが気になります。
うるう年があるかどうかでカレンダーの振る舞いは変わってほしいです。
場合によっては、週の初めを日曜にするか月曜にするかを変えられるオプションもあればよいですね。

このように、設計図から生成するオブジェクト毎に若干の変化を持たせたい場合、初期化メソッドを使います。
初期化メソッドは、特殊なメソッド名 `__init__` で定義します。
`init` の左右にアンダーバー `_` が 2 つ続いていることに注意してください。
両端の 2 つのアンダーバー `_` (ダブルアンダーバー、略してダンダーと呼ぶ人もいます) に挟まれたメソッドを特殊メソッドと呼びます。
他には、`__add__` や `__del__` など様々なものがありますが、現時点で覚える必要はありません。

先ほどのカレンダークラス `Calender` の `year` 属性を変更可能にします。
`__init__` メソッドも他のメソッドと同様、最初の引数にはオブジェクト自身 (`self`) が入ります。
その後に、追加の引数を加えます。
クラスで同じ属性を **クラス属性** (class attribute)、インスタンスによって異なる属性を **インスタンス属性** (instance attribute) と呼びます。

```py
class Calender:

    def __init__(self, year):
        self.year = year

    def print_current_year(self): # メソッドの定義
        print(self.year) # オブジェクト内の `year` を参照

    def print_future_year(self, year_length):
        print(self.year + year_length)

calender = Calender(2020) # インスタンス作成
print(calender.year) # 属性呼び出し
print(calender.print_current_year()) # メソッド呼び出し
print(calender.print_current_year(10)) 
```

実行すると次を返します。

```bash
python class_example.py
2020
2020
2030
```

注意するべきことが 3 つあります。

1. インスタンス属性の紐づけ方法
   
   `Calender` のインスタンスに `<name>` という属性を追加するには、`__init__` メソッド内で `self.<name> = value` が必要です。
   `__init__` メソッドの引数に入れただけでは、オブジェクトのデータとして加わりません。

    例えば、次の `FixedCalender` クラスは、初期化メソッドに `year` という引数を取り入れてますが、それをインスタンス属性に加えていません。

    ```py
    >>> class FixedCalender:
    ...     def __init__(self, year):
    ...         pass
    ...
    >>> fixed_calender = FixedCalender(2020) # インスタンス作成
    >>> print(fixed_calender.year) # 属性呼び出し (エラー)
    Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
    AttributeError: 'FixedCalender' object has no attribute 'year'
    ```

    インデントブロック内の処理が空行だとエラーになるので、`pass` を入れています。
    `pass` は何もしないということと同じです。

    `AttributeError` はオブジェクトに指定した属性がないときに返ってくるエラーです。
    `FixedCalender` には `year` という属性がないことを示しています。

2. 初期化メソッドの呼び出し

    初期化メソッドは、クラスからインスタンスを作成するときに呼ばれます。
    `Calender` クラスからインスタンスを作成する場合、最初は `Calender()` としていましたが、今回は `Calender(2020)` としています。
    メソッドと同様、`__init__` メソッドで `year` という新しい引数を定義したため、`year` に `2020` を加えています。
    `__init__` メソッドに入れる引数が多くなれば、`Calender(arg1, arg2, ...)` と増えていきます。

3. 初期化メソッドの返り値

    初期化メソッドには `return` は入れません。
    `<ClassName>(arg)` でインスタンスを作成した場合にインスタンスが返ってくるからです。
    初期化メソッド内に `return` を入れるとエラーが返ってきます。

    ```py
    >>> class ErrorCalender:
    ...     def __init__(self, year):
    ...         return year
    ...
    >>> ErrorCalender(2020) # インスタンス作成 (エラー)
    Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
    TypeError: __init__() should return None, not 'int'
    ```

> [!note|label:Question]
> 次のような `HealthReport` クラスを作成してみましょう。
> 1. `height`, `weight` というインスタンス属性がある
> 2. `calc_BMI` という BMI を計算するメソッドがある
> 3. `classify_obesity` という肥満度カテゴリー分けをするメソッドがある

