# オブジェクト

ここでは、プログラミングにおいて頻発するオブジェクトという概念について学びます。

## キーワード

- オブジェクト
- 型 (クラス)
- 属性・メソッド

## オブジェクトとは

Python において、数値や、文字列、辞書、関数、その他にリストやクラスなどをまとめて **オブジェクト** として扱います。
すべてのオブジェクトは同一性、型、値をもっています。

### 同一性

同一性とは、そのオブジェクトが他のオブジェクトと区別可能であるということを表しています。
Python では `id` 関数を用いてそのオブジェクトのアイデンティティ (整数) を取得することが可能です。

```py
>>> id(3)
9764448

>>> id("japan")
140191388984560
```

### 型 (クラス)

型 (またはクラス) とは、オブジェクトの種類を指します。
例えば、今まで用いてきた `3`、`4` などの整数は整数 (`int`) 型に分類されます。
`3.14` などの小数点は小数点 (`float`) 型です。
整数型、小数点型などをまとめて数値型とも呼びます。
`"japan"` などの文字列は文字列 (`str`) 型です。
辞書は辞書 (`dict`) 型に属します。
オブジェクトの型は `type` 関数によって調べることができます。

```py
>>> type(3)
<class 'int'>

>>> type(3.14)
<class 'float'>

>>> type("japan")
<class 'str'>
```

### 値

値は、そのオブジェクトが持つ情報で、整数オブジェクトならその値です。
例えば、`3` の値は 3、`japan` の値は japan です。
辞書型では、キーと値のペアの集まりが、その辞書型オブジェクトの持つ値です。

### 属性・メソッド

オブジェクトの型によって、保持している状態を表す変数や用いることができる関数などがあります。
変数のことを **属性** (attribute) 、関数のことを **メソッド** (method) といいます。
これらを参照するには、オブジェクトの後に `.` をつけて、その後に属性名、メソッドを続けます。

例えば、文字列 (`str`) 型には、文字を大文字にする `upper` や逆に小文字にする `lower` メソッドなどがあります。
単語の先頭の文字のみを大文字にする `title` メソッドというのもあります。

> [!note]
> 日本語では大文字・小文字の対応はありません。
> つまり、「や」と「ゃ」は別の文字として扱われています。

```py
>>> "japan".upper()
'JAPAN'

>>> "Hello!".lower()
'hello!'

>>> "this is a pen".title()
'This Is A Pen'
```

オブジェクトにどのような属性・メソッドが備わっているかは、`dir` 関数によって調べることができます。
各メソッドや属性の内容を覚える必要はありませんが、どういったことができるかを知っておくと、困ったときに役立ちます。
多くの場合、属性やメソッドに関するドキュメントが用意されており、google 検索などで調べることが可能です
(例: [`str` 型に関する説明ページ](https://docs.python.org/ja/3.11/library/stdtypes.html#string-methods))。


```py
>>> dir("japan")
['__add__', '__class__', '__contains__', '__delattr__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__getitem__', '__getnewargs__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__iter__', '__le__', '__len__', '__lt__', '__mod__', '__mul__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__rmod__', '__rmul__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', 'capitalize', 'casefold', 'center', 'count', 'encode', 'endswith', 'expandtabs', 'find', 'format', 'format_map', 'index', 'isalnum', 'isalpha', 'isascii', 'isdecimal', 'isdigit', 'isidentifier', 'islower', 'isnumeric', 'isprintable', 'isspace', 'istitle', 'isupper', 'join', 'ljust', 'lower', 'lstrip', 'maketrans', 'partition', 'replace', 'rfind', 'rindex', 'rjust', 'rpartition', 'rsplit', 'rstrip', 'split', 'splitlines', 'startswith', 'strip', 'swapcase', 'title', 'translate', 'upper', 'zfill']
```

