# Python チュートリアル: BMI 計算プログラムの続き

前回の続きで、BMI の計算プログラムを作っていきます。

## キーワード

- 標準出力
- 条件分岐
- 関数
- ローカル変数
- 辞書

## 補足: 名前の規則

変数や後ほど学ぶ関数・クラスなどの名前 (識別子ともいう) は以下のルールを満たす必要があります。

1. アルファベット (a-z, A-Z)、数字 (0-9)、漢字などの Unicode 文字や アンダーバー `_` のみを使っている
2. 先頭が数字ではない
3. 予約語ではない (`and` や `None` など)

漢字が使えるので、以下はエラーになりません。

```py
>>> 体重 = 50
```

識別子の間にスペースは空けられません。
以下はエラーになります。

```py
>>> normal weight = 50
  File "<stdin>", line 1
    normal weight = 50
           ^
SyntaxError: invalid syntax
```

単語をつなげたいときは、アンダーバー `_` を使いましょう

```py
>>> normal_weight = 50
```

## 前回のおさらい

- 目的: 体重・身長が与えられた場合に BMI と肥満度カテゴリーを出力したい
- ファイル: `bmi.py`

- やることリスト
  - BMI を計算する
  - BMI から肥満度に分類する
  - BMI、肥満度を出力

### 補足

体重 $kg$、身長 $m$ に対し、BMI は次のように定義されます。

$$
\textrm{BMI} = kg / m^2
$$

日本肥満学会の出している[ガイドライン](http://www.jasso.or.jp/data/magazine/pdf/medicareguide2022_2.pdf)によると、肥満度は BMI によって次のように分類されます。

$$
\begin{aligned}
\textrm{Underweight}&:& BMI < 18.5 \\
\textrm{Normal} &:& 18.5 \le BMI < 25 \\
\textrm{Overweight} &:& 25 \le BMI
\end{aligned}
$$

してみます。

## 標準出力

上記のコードを `bmi.py` に記載します。
変数にまとめてみます。

```python
# BMI を計算する
BMI = 70 / 1.7 ** 2

# BMI から肥満度に分類する

# BMI、肥満度を出力
BMI
```

スクリプトを実行してみましょう。

```bash
python3 bmi.py
```

何も出力されません。
コマンドプロンプトに結果を出力するには、`print` 関数を用います。
関数は引数を入れると何らかの値を返す箱みたいなものです。
関数を実行するには、変数の時のように関数名を入力するだけでは足りません。
その場合は、関数自体が参照されます。

```py
>>> print
<built-in function print>
```

関数を実行するには、`関数名` の後 (スペースで離さない) に括弧 `()` を入力し、この括弧内に引数を入力します。
実際に `print` 関数に適当な値を入れてみます。

```py
>>> print("Hello World!")
Hello World!
```

`print` 関数には数値を入れても文字として変換されます。
また、`2 + 2` などのような式を入れると、計算結果 (つまり `4`) が表示されます。

```py
>>> print(2 + 2)
4
```

では、`bmi.py` を書き換えて、BMI の計算結果を出力してみます。

```python
# BMI を計算する
BMI = 70 / 1.7 ** 2

# BMI から肥満度に分類する

# BMI、肥満度を出力
print(BMI)
```

実行すると、結果がコマンドプロンプトに出力されます。

```bash
python3 bmi.py
24.221453287197235
```

BMI の計算ができたので、BMI の値によって肥満度を返すロジックを組み込みたいです。
例えば、先の例では BMI は 21 なので、肥満度 Normal を返すようなロジックです。
つまり、BMI の数値に応じて文字列を返すことを行います。

## 条件分岐

条件分岐は `if ~ else` で行います。
Python は他のプログラミング言語と異なり、インデントに意味があり、使える場面が限られます。
インデントは、行の初めに空白を入れて文字を右にシフトさせることです。
段落の初めを 1 文字空けるのものインデントの例です。

例えば、Python では `3` はエラーになりませんが、その前に空白があるとエラーになります。

```python
>>> 3
3
>>>     3
  File "<stdin>", line 1
    3
    ^
IndentationError: unexpected indent
```

インデントは `if ~ else` などの場面で用います。
例えば、次のコードは、`<condition>` が真ならば `operation 1` が実行されます。
偽の場合は `operation 2` が実行されます。
`if <condition>`, `else` の後に `:` を使うことに注意してください。

```python
if <condition>:
    operation 1

else:
    operation 2
```

下のコードの場合、変数 `A` は 2 より大きいので `1` が出力されます。

```python
>>> A = 3
>>> if A > 2:
...     print(1)
... else:
...     print(2)
... 
1
```

> [!tip]
> 文法上は 1 つ以上のスペースでインデントが可能ですが、Python では 4 つのスペースでインデントするのが推奨されています。
> https://pep8-ja.readthedocs.io/ja/latest/#section-4

`operation 1` や `operation 2` は 1 行で書かれている必要はなく、複数の行のコードをまとめたブロックが実行されます。
このブロックはインデントで表現します。
つまり、`operation 1` や `operation 2` が終わりであることを示したい場合には、インデントをやめればよいです。
REPL モードだと、インデントをやめてから空白行を入力すると、条件分岐の実行が終了します。

```python
>>> B = 10
>>> if B < 2:
...     print(1)
... else:
...     print(2)
...     print(3)
... 
2
3
```

`else` は省略可能です。
その場合、条件式が真でない場合は無視されます。

```python
>>> if 3 < 2:
...     print(1)
...

```

条件分岐を増やす場合は、`elif` を使います。
例えば、`C > 10` かどうかを判定した後、`C < 0` なら別のコードを実行したい場合は次のようになります。

```python
>>> C = -5
>>> if C > 10:
...     print("C > 10")
... elif C < 0:
...     print("C < 0")
... else:
...     print("0 <= C <= 10")
...
C < 10
```

注意点として、条件分岐は上から評価され、条件が初めに真になったところで条件分岐は終了します。
細かい条件を最初に書かないと、意図しない挙動になります。

```python
>>> D = 15
>>> if D > 5:
...     print("D > 5")
... elif D > 10:
...     print("D > 10")
... else:
...     print("D <= 5")
...
D > 5
```

```python
>>> D = 15
>>> if D > 10:
...     print("D > 10")
... elif D > 5:
...     print("D > 5")
... else:
...     print("D <= 5")
...
D > 10
```

> [!note|label:Question]
> ある数字が変数 `A` に割り当てられているとして、次の条件式を作成してみましょう。
> `A` が 10 より大きければ、`large` と出力する
> `A` が 5 より小さいならば、 `small` と出力する
> `A` が 11 と同じならば、`eleven` と出力する
> それ以外の場合、`other` と出力する 

では、BMI に基づいて肥満度カテゴリーを出力するロジックを実装します。
BMI の値に応じて、`CATEGORY` という肥満度を表す変数を定義します。
`bmi.py` に以下のコードを追記します。

```python
# BMI を計算する
BMI = 70 / 1.70 ** 2

# BMI から肥満度に分類する
if BMI > 25:
    CATEGORY = "Overweight"

elif BMI < 18.5:
    CATEGORY = "Underweight"

else:
    CATEGORY = "Normal"

# BMI、肥満度を出力
print(BMI)
print(CATEGORY)
```

実行すると、次の結果を得られます。

```bash
python3 bmi.py
24.221453287197235
Normal
```

## 関数

次に、以下の 3 パターンの BMI に対して肥満度カテゴリーを出力してみましょう。
新しいファイル（`bmi_func.py` とします）を作成します。

1. A さん: BMI = 30.5
2. B さん: BMI = 16.7
3. C さん: BMI = 23.4

```python
# BMI
BMI_A = 30.5
BMI_B = 16.7
BMI_C = 23.4

# 結果出力
print(BMI_A)
print(BMI_B)
print(BMI_C)
```

実行すると以下を得ます。

```bash
python3 bmi_func.py
30.5
16.7
23.4
```

各 BMI の肥満度カテゴリーを出力するために、同じロジックを実装するのは面倒です。
関数を作成することによって、使い回しすることができます。
BMI を入力すると、肥満度カテゴリーを出力する関数を作成します。

関数は `def` 構文を使用します。
関数で行うソースコードは、`if ~ else` の時と同様、インデントしたブロック内に記載します。

```python
def func_name(arg_name):
    return value
```

- `func_name` は関数名、`arg_name` は引数名です。
- `return` によって関数から値を出力します。

> [!NOTE] 
> 関数内で `return` を定義していない場合、`None` が出力されます。
> 初心者がやりがちなミスなので、注意してください。

`arg_name` は省略可能です。
省略した場合、関数は同じ処理しか行いません。

```python
>>> def get_one():
...     print("print 1")
...     return 1
...
```

`print` は関数内で実行されてもコマンドプロンプトに出力されます。
上で定義した関数は次のように実行します。
さきほど説明したように、`()` をつけないと関数を呼び出すだけになることに注意してください。

```python
>>> get_one()
print 1
1
>>> get_one
<function get_one at 0x7fe6f01de700>
```

`arg_name` を定義した場合、`()` に入れた値は `arg_name` という変数に割り当てられます。
以下の例では、`3` が `x` に割り当てられていることがわかります。

```python
>>> def add_two(x):
...     return x + 2
...
>>> add_two(3)
5
```

次の例では、引数名では `x` としてるのに、関数内で定義していない `y` を参照しているのでエラーになっています。

```py
>>> def invalid_name(x):
...     return y + 2
...
>>> invalid_name(3)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "<stdin>", line 2, in invalid_name
NameError: name 'y' is not defined
```

引数の数は複数にすることができます。
引数名は `,` で区切ります。

```python
>>> def sum_values(x, y):
...     return x + y
...
>>> sum_values(3, 5)
8
```

引数の入力の仕方は 2 種類あります。
値を入力しただけの場合、左から順番に値が割り当てられます (positional argument: 位置固定引数)。
先の関数を少し変更して、各変数になんの値が割り当てられているかを出力します。

```python
>>> def sum_values(x, y):
...     print("x:", x)
...     print("y:", y)
...     return x + y
...
```

```python
>>> sum_values(3, 5)
x: 3
y: 5
8
```

引数名を指定すると、対応する引数名に値が割り当てられます (keyword argument: キーワード引数)。

```python
>>> sum_values(y = 3, x = 5)
x: 5
y: 3
8
```

positional argument は keyword argument の前にする必要があります。

```python
>>> sum_values(x = 3, 5)
  File "<stdin>", line 1
SyntaxError: positional argument follows keyword argument
```

そのため、次もエラーになります。
これは、positional argument によって `x = 5` が割り当てられているにも関わらず、さらに `x = 3` を割り当てられいるからです。

```python
>>> sum_values(5, x = 3)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: sum_values() got multiple values for argument 'x'
```

`return` が実行されると、その後のコードは無視されます。

```python
>>> def check_return():
...     print("above")
...     return "return"
...     print("below")
...
check_return()
above
'return'
```

> [!note|label:Question]
> 次の関数を作成してみましょう
> cm 基準の身長を m 単位に変換する関数
> x, y を入力すると、x/y を返す関数

### ローカル変数

関数内で定義した変数は関数内でしか参照できません。
関数外で定義した変数は関数内でも参照できます。
こういった、変数の参照可能な範囲をスコープといいます。
また、関数内で定義した変数をローカル変数、関数外で定義した変数をグローバル変数といいます。
以下の例では、`K` という変数を関数内外で定義していますが、関数を実行しても関数外の変数には影響していません。

```python
>>> def define_K(c):
...     K = c + 5
...     return K
...
>>> K = 10
>>> K
10
>>> define_K(3)
8
>>> K
10
```

```python
>>> def call_global():
...     print(K)
...
>>> K = 5
>>> call_global()
5
```

一見ややこしく感じるかもしれませんが、これはとても役立ちます。
変数や関数を多く定義した場合、このような仕組みがないと、変数内の値が意図せず上書きされてしまう恐れがあります。
もしこの仕組みがなかったらどうなるでしょうか。
例えば、データ分析をしていて、t 検定を行いたいとします。
t 検定には [`scipy`](https://docs.scipy.org/doc/scipy/index.html) というパッケージの [`ttest_ind`](https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.ttest_ind.html) という関数が使えます (パッケージについては後ほど説明します)。
もし分析したいデータの変数名を `df` としていた場合、ローカル変数の仕組みがないと、実は `ttest_ind` を実行すると `df` も書き換わってしまいます。
なぜなら、この関数の中で `df` という変数が定義されているからです。
したがって、意図せず変数を書き換えないように、全ての関数内外で、変数名が被らないようにコードを読み解いて、覚えておく必要があります (`ttest_ind` のソースコードはこちら: [URL](https://github.com/scipy/scipy/blob/v1.11.4/scipy/stats/_stats_py.py#L7174-L7479))。
もちろんのこと、これはバグの温床ですし、現実的でもありません。
ローカル変数などの仕組みによって、そのような心配をせずに関数の影響を最小限に留めておけるわけです。

> [!TIP]
> 一応、グローバル変数は関数内で呼び出すことができますが、基本的に推奨はされていません。
> グローバル変数が変わった場合にどこが変化するかを追いきれなくなる可能性があるためです。

さて、`bmi_func.py` にカテゴリーを出力する関数を定義して、結果を出力しましょう。

```python
def classify_BMI(bmi):
    if bmi > 25:
        return "Overweight"

    elif bmi < 18.5:
        return "Underweight"
    
    else:
        return "Normal"

# 結果出力
print(BMI_A, classify_BMI(BMI_A))
print(BMI_B, classify_BMI(BMI_B))
print(BMI_C, classify_BMI(BMI_C))
```

`print` 関数は `,` で区切るとまとめて出力できます。

> [!TIP]
> 関数の名前は、`func1` などといった意味のないものではなく、分かりやすい名前にしましょう。

実行すると以下を得られます。

```bash
python3 bmi_func.py
30.5 Overweight
16.7 Underweight
23.4 Normal
```

ついでに、身長・体重が与えられた場合に、BMI を計算する関数も定義しましょう。

```python
>>> def calc_BMI(height, weight):
...     return weight / height ** 2
...
>>> calc_BMI(1.78, 70)
21.461936624163616
```

`bmi_func.py` にも追記してください。

## 辞書

A、B、C さんの身長・体重はそれぞれ以下の通りです。

1. A さん: BMI = 30.5
   - 身長 = 1.64 m
   - 体重 = 82 kg
2. B さん: BMI = 16.7
   - 身長 = 1.83 m
   - 体重 = 56 kg
3. C さん: BMI = 23.4
   - 身長 = 1.60 m
   - 体重 = 60 kg

A さんの身長・体重を分けて変数として定義することもできますが、まとめた方がすっきりしそうです。
**辞書** (dictionary) というものは、このような用途に使えます。
Python では波括弧 `{}` によって辞書を作成します。
辞書では、`key` と `value` をセットで登録する必要があります。
`key` は呼び出し用の名前で、`value` はその値です。
`{key: value}` で定義します。

> [!tip]
> キー (key) と値 (value) のペアを `key-value` とまとめて表現します

`key` には数値型や文字列型などが使用できます。

```python
>>> {"apple": 1}
{'apple': 1}
```

複数の `key-value` をセットするときは、`,` で区切ります。

```python
>>> {"apple": 1, "lemon": 2}
{'apple': 1, 'lemon': 2}
```

`key` の値は一意である必要があることに注意してください。
後に定義されたものに上書きされます。

```python
>>> {"apple": 1, "apple": 2}
{'apple': 2}
```

定義した辞書から値を呼び出すのは `dict_name[key]` で行えます。

```python
>>> fruits = {"apple": 1}
>>> fruits
{'apple': 1}
>>> fruits["apple"]
1
```

登録されていない `key` を与えるとエラーになります。

```python
>>> fruits["melon"]
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
KeyError: 'melon'
```

`dict_name[key] = value` で新たに `key-value` を登録できます。
`key` がすでに存在している場合は上書きされます。

```python
>>> fruits["melon"] = 3
>>> fruits
{'apple': 1, 'melon': 3}
>>> fruits["apple"] = 2
>>> fruits
{'apple': 2, 'melon': 3}
```

A さん、B さん、C さんの身長・体重を辞書を使って `bmi_func.py` 内に定義してみましょう。

```python
# 身長・体重
A = {"height": 1.64, "weight": 82}
B = {"height": 1.83, "weight": 56}
C = {"height": 1.60, "weight": 60}
```

以前定義した BMI の値を身長・体重から計算して置き換えましょう。

```python
# BMI
BMI_A = calc_BMI(A["height"], A["weight"])
BMI_B = calc_BMI(B["height"], B["weight"])
BMI_C = calc_BMI(C["height"], C["weight"])
```

実行すると以下を得ます。

```bash
python3 bmi_func.py
30.487804878048784 Overweight
16.72190870643435 Underweight
23.437499999999996 Normal
```

BMI の小数点が細かすぎるので、`round` 関数によって値を丸めます。
`round(value, precision)` とすると、`value` は小数点 `precision` 桁で丸められます。
`print` 関数部分を変更します。

```python
# 結果出力
print(round(BMI_A, 2), classify_BMI(BMI_A))
print(round(BMI_B, 2), classify_BMI(BMI_B))
print(round(BMI_C, 2), classify_BMI(BMI_C))
```

実行結果は以下の通りです。

```bash
python3 bmi_func.py
30.49 Overweight
16.72 Underweight
23.44 Normal
```

## 補足

### 辞書の入れ子と json
辞書の中に辞書を入れることも可能です。
例えば、先ほどの A, B, C さんの身長・体重を保存したデータを 1 つの辞書にまとめることができます。

```py
data = {
  "A": {
    "height": 1.64, 
    "weight": 82
  },
  "B": {
    "height": 1.83,
    "weight": 56
  },
  "C": {
    "height": 1.60, 
    "weight": 60
  }
}
```

C さんの健康診断データのみ、身長・体重だけでなく血糖値のデータもある場合、そこだけ拡張することもできます。

```py
data = {
  "A": {
    "height": 1.64, 
    "weight": 82
  },
  "B": {
    "height": 1.83,
    "weight": 56
  },
  "C": {
    "height": 1.60, 
    "weight": 60,
    "BGL": 85, # BGL は blood glucose level の略
  }
}
```

このようなデータの形式は `json` (JavaScript Object Notation) というファイル形式によく似ています。
実際、辞書データと `json` 形式には互換性があります。
json ファイルはよく Web サイト上で使用するデータに用いられます。
例えば、Stack Overflow などの Q & A サイトをまとめている StackExchange からデータをダウンロードできます ([リンク](https://api.stackexchange.com/docs))。
ここから、answers というリンクをクリックして、`Run` というボタンを削除されてない最近の解答情報を取得して確認してみましょう ([リンク](https://api.stackexchange.com/docs/answers))。

逆に、1 回の推定に時間がかかるデータ分析の結果を以下のような辞書にまとめ、`json` ファイルとして保存しておくといった用途もあります。

```py
{
  "n": 100000,
  "k": 50,
  "var1": {
    "coef": 0.123,
    "se": 0.111,
  },
  .
  .
  .
}

```