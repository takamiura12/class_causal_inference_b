# Pandas 応用編

ここでは、[`pandas`](https://pandas.pydata.org/docs/index.html) のさらに応用について学んでいきます。

- [Pandas 応用編](#pandas-応用編)
  - [キーワード](#キーワード)
  - [前処理](#前処理)
  - [欠損値処理](#欠損値処理)
    - [欠損値とは](#欠損値とは)
    - [欠損値の確認](#欠損値の確認)
    - [欠損値の穴埋め](#欠損値の穴埋め)
    - [欠損データの除外](#欠損データの除外)
  - [データ型変換](#データ型変換)
  - [文字列操作](#文字列操作)
  - [日付・時間型データの操作](#日付時間型データの操作)
  - [データの操作](#データの操作)
    - [四則演算](#四則演算)
    - [条件式と条件分岐](#条件式と条件分岐)
    - [`isin` メソッド](#isin-メソッド)
  - [より高度なフィルタリング](#より高度なフィルタリング)
  - [データの結合](#データの結合)
  - [ピボットテーブル](#ピボットテーブル)
  - [データ出力](#データ出力)
  - [ソート・ランク](#ソートランク)
  - [データの確認・整理につかう関数・メソッド](#データの確認整理につかう関数メソッド)

## キーワード

- 前処理
- 欠損値
- `str` アクセサ
- 時間型
- 条件分岐
- データの結合
- データの出力
- ソート・ランク

## 前処理

何らかの方法で分析に用いるデータを入手したとして、それをそのまま使えるわけではありません。
データの加工や整形などを行う必要があり、これらを **前処理** (preprocessing) といったりします。
ここでは、そのうちのいくつかのトピックを `pandas` を使って学びます。

> [!note]
> この前処理を実装するのはとても時間がかかることが多いです。
> データの細かい部分まで理解しておく必要があるからです。
> また、前処理には非常に多くのトピックがあり、[前処理大全](https://gihyo.jp/book/2018/978-4-7741-9647-3) という本もあります。

## 欠損値処理

### 欠損値とは

特別な場合を除いて、全ての行・カラムにデータの値が入っていることはありません。
例えば、収入に対するアンケート項目には答えなくない人もいるでしょうし、機器の故障で特定の地域のデータが入らないこともあります。
これをまとめて **欠損値** (missing value) と呼びます。
必要に応じて、欠損値に対する処理を行うことがあります。

以下は、[e-stat](https://www.e-stat.go.jp/) からダウンロードした、2020 年度の産業別雇用数増減の[データ](https://www.e-stat.go.jp/dbview?sid=0003225718) の内訳を示しています。

```py
>>> import pandas as pd
>>> pd.read_csv("data/japan_employee_change_2020.csv")
      年度次       産業  /雇用者数の増減率 回答企業数【社】  △１５％以下【％】  △１５％超△１０％以下【％】  △１０％超△５％以下【％】  △５％超０％未満【％】  ０％【％】  ０％超５％未満【％】  ５％以上１０％未満【％】  １０％以上１５％未満【％】  １５％以上【％】  階級値平均【％】
0  2020年度      全産業        NaN    1,108        1.8             0.3            3.2         18.0   10.6        46.8          11.8            3.2       4.3      2.11
1  2020年度      製造業        NaN      498        0.8             0.2            2.8         18.5   12.4        49.6          10.2            2.2       3.2      1.95
2  2020年度   素材型製造業        NaN      148        1.4             NaN            2.0         16.2   11.5        53.4          10.1            2.0       3.4      2.09
3  2020年度   加工型製造業        NaN      203        0.5             0.5            2.5         20.2   10.3        50.7          10.3            3.0       2.0      1.88
4  2020年度  その他の製造業        NaN      147        0.7             NaN            4.1         18.4   16.3        44.2          10.2            1.4       4.8      1.89
5  2020年度     非製造業        NaN      610        2.6             0.3            3.4         17.5    9.2        44.6          13.1            3.9       5.2      2.25
```

`△１５％超△１０％以下【％】` というカラムに注目してください、数値以外に、`NaN` という文字が表示されているのが見えます。
`NaN` という表記は、文字列として `NaN` というものが入っているわけではありません。
Nan は Not A Number の略で、数値ではないことを表しています。

### 欠損値の確認

値が欠損値かどうかを調べるには、[`isna`](https://pandas.pydata.org/docs/reference/api/pandas.isna.html) メソッドまたは [`isnull`](https://pandas.pydata.org/docs/reference/api/pandas.isnull.html) メソッドを使います (どちらも同じ機能)。
これは、欠損値 (`None` も含みます) なら `True`、そうでないなら `False` を返します。
`DataFrame` と `Series` どちらにも実装されていますが、注目したいカラムの `Series` で使ってみます。

```py
>>> import pandas as pd
>>> data = pd.read_csv("data/japan_employee_change_2020.csv")
>>> COL_NAME = "△１５％超△１０％以下【％】" # 注目したいカラム
>>> data[COL_NAME] # データの参照
0    0.3
1    0.2
2    NaN
3    0.5
4    NaN
5    0.3
Name: △１５％超△１０％以下【％】, dtype: float64

>>> data[COL_NAME].isnull() # 欠損値かどうかの判定
0    False
1    False
2     True
3    False
4     True
5    False
Name: △１５％超△１０％以下【％】, dtype: bool
```

例えば、これに `sum` メソッドを適用すると、欠損値の数が分かります。

```py
>>> data[COL_NAME].isnull().sum() # 欠損値の合計
2
```

> [!tip]
> メソッドがオブジェクトを返す場合、さらにそのオブジェクトのメソッドを続けて適用することができます。
> これをメソッドチェーンといいます。
> 上の例では、`isnull` メソッドが `Series` オブジェクトを返すので、続けて `Series` の `sum` メソッドを適用しています。
> メソッドチェーンを行うことで、短いコードで様々な操作を組み合わせることが可能です。

逆に、欠損値ではないことを確かめるには、[`notna`](https://pandas.pydata.org/docs/reference/api/pandas.notna.html#pandas.notna) メソッドか [`notnull`](https://pandas.pydata.org/docs/reference/api/pandas.notnull.html) メソッドを用います。
欠損値ならば `False`、欠損値でないならば、`True` を返します。

```py
>>> data[COL_NAME].notnull() # 欠損値ではないかの判定
0     True
1     True
2    False
3     True
4    False
5     True
Name: △１５％超△１０％以下【％】, dtype: bool
```

> [!tip]
> `True` や `False` を数値として扱う場合、`True` は 1, `False` は 0 として扱います。

### 欠損値の穴埋め

欠損値を扱うときは、*なぜ* 欠損値になっているかを考える必要があります。
基本的には、データ公開元に欠損値の規則が記載されていることが多いです。
例えば、e-stat では、`凡例表示` という部分をクリックすると、欠損値の規則が記載されています。
ここでの欠損値は、「皆無または定義上該当数字がないもの」ということでした。
要するに、この例では 0 で場合には欠損値になるようです。

平均値などを計算する場合、`pandas` ではデフォルトで欠損値を無視して計算します。
実際に見てみましょう。

```py
>>> data[COL_NAME]
0    0.3
1    0.2
2    NaN
3    0.5
4    NaN
5    0.3

>>> data.mean()
0.325
```

欠損値でない値の平均は、$(0.3 + 0.2 + 0.5 + 0.3)/4 = 0.325$ なので、たしかに欠損値を無視していることがわかりました。

> [!tip]
> メソッドの引数で `skipna = False` を指定すると、データに欠損値がある場合には `nan` を返します。

ここでは、実際には欠損値は `0` なので、この平均値は過大に推定されています。
欠損値を別の値に穴埋めするには、`fillna` メソッドを用います。
`fillna` メソッドには、欠損値に埋める値 (`value`) を指定するか、`method` キーワードで、そのデータの直前 (上の行) の値 (欠損値ではないもの) を埋めるか (`ffill`)、直後の値を埋めるか (`bfill`) を指定します。

`0` で欠損値を埋めたいので、次のようにします。

```py
>>> data[COL_NAME].fillna(0) # 欠損値を 0 で穴埋め
>>> data[COL_NAME].fillna(0)
0    0.3
1    0.2
2    0.0
3    0.5
4    0.0
5    0.3
Name: △１５％超△１０％以下【％】, dtype: float64
```

これで、正確な平均値が計算できるようになりました。

```py
>>> data[COL_NAME].fillna(0).mean()
0.21666666666666667
```

### 欠損データの除外

欠損値が、例えばアンケートの未回答であることを表す場合、データから欠損値がある行 (サンプル) を除くことがあります。
`Series` にも同じメソッドがありますが、`DataFrame` だと [`dropna`](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.dropna.html) メソッドを用いることで、欠損値がある *行* を除外できます。

`subset` キーワードで指定することで、特定のカラムで欠損値がある場合のみデータを除外するようにできます (カラムなどで複数指定可能)。
また、指定した全てのカラムで欠損値がある場合のみ除外するか (`how` キーワードで指定)、一定以上のデータの欠損がある場合にみ除外するか (`thresh` キーワードで指定) など、細かい設定が可能です。
逆に、カラムの除外もできます (`axis` キーワードで指定)。

> [!note]
> ここでは詳しく扱いませんが、データを除外するかどうかには細心の注意を払ってください。
> データを除外してしまうことで、意図しない偏り (バイアス) のある結果になる可能性があります。

`dropna` メソッドを使うと、カラム `△１５％超△１０％以下【％】` のインデックス `2` と `4` のデータが除外された結果を返します。

```py
>>> import pandas as pd
>>> data = pd.read_csv("data/japan_employee_change_2020.csv")
>>> COL_NAME = "△１５％超△１０％以下【％】" # 注目したいカラム
>>> data[COL_NAME] # データの参照
0    0.3
1    0.2
2    NaN
3    0.5
4    NaN
5    0.3
Name: △１５％超△１０％以下【％】, dtype: float64

>>> data.dropna(subset = COL_NAME) # △１５％超△１０％以下【％】で欠損値がある行を除外する
      年度次      産業  /雇用者数の増減率 回答企業数【社】  △１５％以下【％】  △１５％超△１０％以下【％】  △１０％超△５％以下【％】  △５％超０％未満【％】  ０％【％】  ０％超５％未満【％】  ５％以上１０％未満【％】  １０％以上１５％未満【％】  １５％以上【％】  階級値平均【％】
0  2020年度     全産業        NaN    1,108        1.8             0.3            3.2         18.0   10.6        46.8          11.8            3.2       4.3      2.11
1  2020年度     製造業        NaN      498        0.8             0.2            2.8         18.5   12.4        49.6          10.2            2.2       3.2      1.95
3  2020年度  加工型製造業        NaN      203        0.5             0.5            2.5         20.2   10.3        50.7          10.3            3.0       2.0      1.88
5  2020年度    非製造業        NaN      610        2.6             0.3            3.4         17.5    9.2        44.6          13.1            3.9       5.2      2.25
```

## データ型変換

`pandas` では、データを読み込んだりして `Series` や `DataFrame` を作成すると、自動でそのデータ型を類推してくれます。
しかし、データの型を後から変換するのが必要な場合もあります。

よくあるのが、欠損値の処理の際です。
時には、欠損値が何らかのシンボルで表されることがあります。
例えば、[e-stat](https://www.e-stat.go.jp/) からデータをダウンロードするとき、デフォルトでは欠損値はハイフン `-` などの記号で表されます。

次は、2020 年度の産業別雇用数増減の[データ](https://www.e-stat.go.jp/dbview?sid=0003225718)を、デフォルトの欠損値のままでダウンロードした場合の結果です。
ファイル名は `employee_change_2020_missing.csv` とします。

```py
>>> import pandas as pd
>>> pd.read_csv("data/employee_change_2020_missing.csv")
      年度次       産業  /雇用者数の増減率 回答企業数【社】  △１５％以下【％】 △１５％超△１０％以下【％】  △１０％超△５％以下【％】  △５％超０％未満【％】  ０％【％】  ０％超５％未満【％】  ５％以上１０％未満【％】  １０％以上１５％未満【％】  １５％以上【％】  階級値平均【％】
0  2020年度      全産業        NaN    1,108        1.8            0.3            3.2         18.0   10.6        46.8          11.8            3.2       4.3      2.11
1  2020年度      製造業        NaN      498        0.8            0.2            2.8         18.5   12.4        49.6          10.2            2.2       3.2      1.95
2  2020年度   素材型製造業        NaN      148        1.4              -            2.0         16.2   11.5        53.4          10.1            2.0       3.4      2.09
3  2020年度   加工型製造業        NaN      203        0.5            0.5            2.5         20.2   10.3        50.7          10.3            3.0       2.0      1.88
4  2020年度  その他の製造業        NaN      147        0.7              -            4.1         18.4   16.3        44.2          10.2            1.4       4.8      1.89
5  2020年度     非製造業        NaN      610        2.6            0.3            3.4         17.5    9.2        44.6          13.1            3.9       5.2      2.25
```

`△１５％超△１０％以下【％】` カラムの、前と例との違いが分かるでしょうか。
この例では、前の例で `NaN` と表記されていたものがハイフン `-` で表示されています。

この違いは、データ型に表れます。
前の例の場合と今回の例の場合での該当のカラムのデータ型を見てみます。

```py
import pandas as pd
>>> COL_NAME = "△１５％超△１０％以下【％】" # 注目したいカラム名
>>> data_symbolic = pd.read_csv("data/employee_change_2020_missing.csv") # 欠損値がハイフンの場合
>>> data_symbolic[COL_NAME].dtype
dtype('O')

>>> data_nonsymbolic = pd.read_csv("data/japan_employee_change_2020.csv") # 欠損値が何もなしの場合
>>> data_nonsymbolic[COL_NAME].dtype 
dtype('float64')
```

`dtype('O')` というのは、`object` 型であることを示しています。
つまり、欠損値がハイフン `-` で表されている場合、`object` 型になっており、何も値を入れないことを欠損値として表現した場合は、`float64` 型、つまり小数点のデータ型として扱われています。

なぜ欠損値の処理が重要なのかというと、平均などの集計に `object` 型は使えないからです。

```py
import pandas as pd
>>> COL_NAME = "△１５％超△１０％以下【％】" # 注目したいカラム名
>>> data_symbolic = pd.read_csv("data/employee_change_2020_missing.csv") # 欠損値 = `-`
>>> data_symbolic[COL_NAME].mean() # `object` 型なのでエラー
Traceback (most recent call last):
<略>
TypeError: Could not convert 0.30.2-0.5-0.3 to numeric

>>> data_nonsymbolic = pd.read_csv("data/japan_employee_change_2020.csv") # 欠損値 = ``
>>> data_nonsymbolic[COL_NAME].mean() # 小数点 (float64) 型なので計算可能
0.325
```

まずは、ハイフン `-` は文字列なので、欠損値のシンボルを欠損値 (`NaN`) に変えるか、数値に変える必要があります。
[`replace`](https://pandas.pydata.org/docs/reference/api/pandas.Series.replace.html) メソッドを用いて、指定した対応関係の値の変換を行えます。
対応関係の指定の仕方はいくつかありますが、辞書型でやるのが直感的だと思います。
辞書型でやる場合、データの値 `key` のものが、`value` の値に変換されます。

ここでは、ハイフン `-` を 0 に変換します。

```py
>>> data_symbolic[COL_NAME].replace({"-": 0}) # ハイフンを 0 に変換
0    0.3
1    0.2
2      0
3    0.5
4      0
5    0.3
Name: △１５％超△１０％以下【％】, dtype: object
```

欠損値に変換する場合は、[`numpy`](https://numpy.org/ja/) というライブラリの [`nan`](https://numpy.org/doc/stable/reference/constants.html) オブジェクトを指定します。
`numpy` は一般に `np` と略されてインポートされます。
`pandas` をインストールしたときに自動でインストールされているはずです。

> [!note]
> `numpy` は数値計算に特化した Python のライブラリです。
> 最小二乗法や関数の解を求めるといったことも可能です。
> また、`pandas` もデータの実装に `numpy` を使っています。

```py
>>> import numpy as np
>>> data_symbolic[COL_NAME].replace({"-": np.nan})
0    0.3
1    0.2
2    NaN
3    0.5
4    NaN
5    0.3
Name: △１５％超△１０％以下【％】, dtype: object
```

欠損値の記号を変換したままでは、データ型は `object` のままです。
データ型を変換するには、`Series` の `astype` メソッドでデータ型を指定して変換します。
データ型の指定には、`int` や `str` などの関数か、`dtype` の文字列リテラル (`int64` など) を使います。
`dtype` の種類は次を参照してください。

- `dtype` の種類: https://pandas.pydata.org/docs/user_guide/basics.html#basics-dtypes

例えば、ここでは小数点型に変換したいので、`float` 関数を入れるか、`"float64"` というデータ型の文字列リテラルを入れます。

```py
>>> data_symbolic[COL_NAME].replace({"-": 0}).astype(float) # 関数で指定
0    0.3
1    0.2
2    0.0
3    0.5
4    0.0
5    0.3
Name: △１５％超△１０％以下【％】, dtype: float64

>>> data_symbolic[COL_NAME].replace({"-": 0}).astype("float64") # データ型の文字列で指定
0    0.3
1    0.2
2    0.0
3    0.5
4    0.0
5    0.3
Name: △１５％超△１０％以下【％】, dtype: float64
```

数値型に変換するならば、`pandas` の `to_numeric` 関数 (メソッドではないことに注意!) の方が便利なこともあります。
`to_numeric` 関数は自動で適当な数値型に変換してくれます(小数点を含んでいれば小数点型に、整数だけなら整数型に)。

```py
>>> pd.to_numeric(data_symbolic[COL_NAME].replace({"-": 0})) # 適当な数値型に変換
0    0.3
1    0.2
2    0.0
3    0.5
4    0.0
5    0.3
Name: △１５％超△１０％以下【％】, dtype: float64
```

> [!note]
> 欠損値への変換の場合、`pandas` の [`NA`](https://pandas.pydata.org/docs/user_guide/missing_data.html#na-semantics) オブジェクトを使って欠損値にすることもできますが、少し注意が必要です。
> `NA` は、文字列などにも対応した欠損値表現なので、`object` 型に使うと、文字列の欠損値として扱われます。
> したがって、`astype` で数値に変換しようとするとエラーになります。
> `to_numeric` 関数は、この違いを上手く処理してくれ、数値型の欠損値に変換してくれます。

欠損値のシンボルは、時には記号ではなく数値で表されることもあります。
例えば、1 ~ 5 段階で答えるようなアンケート項目のデータのときに、それよりも大きい値 (基本的には `9` をつけることが多い) を欠損値のシンボルとして表すことがあります。
したがって、まず欠損値がどのように扱われているかをきちんと知ることがデータ分析では重要です。

> [!note]
> アンケートなどを依頼するときなどでは、欠損値の規則もしっかりとルール化し、伝えておく必要があります。
> そうしないと、担当者によって様々なシンボルが使われたりと、後で泣きを見ることになります。

## 文字列操作

時には、欲しい数値が文字列の一部に入っている場合があります。
以下は、サービスの評価を 1 ~ 5 段階で評価してもらった顧客満足度データを表しています。
サービス評価のデータは、尺度の数値とカテゴリー名がくっついたものになっています。
データとしては何を示しているか分かりやすいですが、満足度の平均値を計算するには数値を取り出す必要があります。

```py
>>> import pandas as pd
>>> data = pd.DataFrame({
...     '顧客ID': [101, 102, 103, 104, 105],
...     'サービス評価': ['1.とても悪かった', '2.悪かった', '3.普通', '4.良かった', '5.とても良かった'],
... }) # アンケートデータの例（尺度の数値と尺度のカテゴリー名がくっついている）
>>> data
   顧客ID     サービス評価
0   101  1.とても悪かった
1   102     2.悪かった
2   103       3.普通
3   104     4.良かった
4   105  5.とても良かった
```

`Series` の `str` アクセッサーを使うと、文字列に対する高度な操作が可能です。
`str` アクセッサーは、`Series` オブジェクトに対して、`.str` をつけることで、使用可能になります。
`data` が `Series` オブジェクトとすると、`data.str.<method>` で文字列操作のメソッドが使えます。

文字列操作の機能を使う前に、まずはデータのパターンを読み解きます。
サービス評価の値は、`数値.カテゴリー名` というパターンで表記されています。
したがって、いくつかの方策が考えられるはずです。

1. ドット `.` で文字列をリストに分割し、`0` 番目の要素を取り出す
2. 1 桁の数値しかないので、最初の文字だけを切り出す
3. 文字列の先頭から、数値の部分だけを取り出す
4. 数値以外の文字を空文字 `""` にする

ここでは、最初の方法でやってみます。
文字列のメソッドと同じように、`pandas` でも [`split`](https://pandas.pydata.org/docs/reference/api/pandas.Series.str.split.html) メソッドでデータの文字列を分割できます。
デフォルトだと、文字列を区切り文字で分割したリストの `Series` が返ってきます。

```py
>>> data["サービス評価"].str.split(".") # . で文字列を分割
0    [1, とても悪かった]
1       [2, 悪かった]
2         [3, 普通]
3       [4, 良かった]
4    [5, とても良かった]
Name: サービス評価, dtype: object
```

`str` アクセッサーを使って、`i` 番目の要素を取り出すことができます。
最初の要素を取り出すには、`str[0]` です。
次のように組み合わせることで、求めていた数値が取り出せました。

```py
>>> data["サービス評価"].str.split(".").str[0]
0    1
1    2
2    3
3    4
4    5
Name: サービス評価, dtype: object

```

ちなみに、他の方法でやる場合は以下の方法で可能です。

2. `str[0]` で最初の文字をとる
3. `extract` メソッドでパターンにマッチする要素 (数値) を取り出す
4. `replace` メソッドでパターンにマッチする要素 (数値以外) を空文字 `""` にする

> [!tip]
> ここでは深く触れませんが、パターンを示すには正規表現 (regular expression) というものを用いることで、より広い範囲のパターンを示すことができます。
> 例えば、正規表現では任意の数値を `\d` で示せます。
> `extract(r"(\d))` とすると、先頭に数値がある場合に、それを取り出します。

## 日付・時間型データの操作

ユーザーの登録日時や購入時間などでは、時間のデータ型 `datetime64` を用いるのが便利です。
`datetime64` 型の `Series` の作成は、`pandas` の `to_datetime` 関数を使うことで可能です。

```py
>>> import pandas as pd
>>> data = pd.DataFrame({
...     '顧客ID': [1, 2, 1, 3, 2],
...     '購入日時': ['2023-01-01 10:00', '2023-01-05 15:30', 
...                  '2023-02-01 11:00', '2023-02-05 18:00', 
...                  '2023-03-01 09:30'],
...     '購入金額': [1000, 1500, 2000, 2500, 3000]
... })
>>> data["購入日時"] # object 型
0    2023-01-01 10:00
1    2023-01-05 15:30
2    2023-02-01 11:00
3    2023-02-05 18:00
4    2023-03-01 09:30
Name: 購入日時, dtype: object

>>> pd.to_datetime(data["購入日時"]) # datetime64 型
0   2023-01-01 10:00:00
1   2023-01-05 15:30:00
2   2023-02-01 11:00:00
3   2023-02-05 18:00:00
4   2023-03-01 09:30:00
Name: 購入日時, dtype: datetime64[ns]
```

`dt` アクセッサーを使うことで、年、月、日などの情報を取り出すことができます。

```py
>>> pd.to_datetime(data["購入日時"]).dt.year # 年を取得
0    2023
1    2023
2    2023
3    2023
4    2023
Name: 購入日時, dtype: int32

>>> pd.to_datetime(data["購入日時"]).dt.dayofweek  # 曜日の数字 (月曜が 0 ~ 日曜が 6) 取得
0    6
1    3
2    2
3    6
4    2
Name: 購入日時, dtype: int32
```

また、[`resample`](https://pandas.pydata.org/docs/reference/api/pandas.Series.resample.html) メソッドを使うと、指定した時間の単位でデータを集計してくれます。
`groupby` の時間版だと思ってください。
例えば、日次平均、年次平均を計算することが可能です。
`resample` メソッドでは、集計する期間を指定します。
例えば、`"M"` は月単位を表し、`"Y"` は年単位を表します。
index が時間ではない場合、`on` キーワードで集計に用いる時間のカラム名を指定します。

`resample` で集計した後に、集計メソッドを適用します。

```py
>>> import pandas as pd
>>> data = pd.DataFrame({
...     '顧客ID': [1, 2, 1, 3, 2],
...     '購入日時': pd.to_datetime(['2023-01-01 10:00', '2023-01-05 15:30', 
...                   '2023-02-01 11:00', '2023-02-05 18:00', 
...                   '2023-03-01 09:30']),
...     '購入金額': [1000, 1500, 2000, 2500, 3000]
... })
>>> data.resample("M", on = "購入日時").mean() # 月次平均
            顧客ID    購入金額
購入日時                    
2023-01-31   1.5  1250.0
2023-02-28   2.0  2250.0
2023-03-31   2.0  3000.0

>>> data.resample("Y", on = "購入日時").mean() # 年次平均
            顧客ID    購入金額
購入日時                    
2023-12-31   1.8  2000.0
```

さらに、`"5Min"` とすれば 5 分毎に集計するなど、柔軟な指定が可能です。
詳しくは、`pandas` のドキュメントを読んでください。
- https://pandas.pydata.org/pandas-docs/stable/user_guide/timeseries.html

## データの操作

ここでは、データの加工について少し触れていきます。

### 四則演算

`Series`, `DataFrame` 同士で四則演算をすることが可能です。
`Series` 同士の場合は、インデックスが同じ要素同士で演算が行われます。
長さが異なるもの同士でも演算が可能ですが、片方しかない要素の計算結果は、`NaN` が入ります。
両方のインデックスが組み合わさった `Series` が計算結果として返されます。

```py
>>> l = pd.Series([1, 2, 3], index = ["A", "B", "C"])
>>> l
A    1
B    2
C    3
dtype: int64

>>> l + l # 同じインデックス同士で足し算
A    2
B    4
C    6
dtype: int64

>>> l + pd.Series([1, 3, 2], index = ["A", "C", "B"]) # インデックスの順番は影響しない
A    2
B    4
C    6
dtype: int64

>>> l + pd.Series([1, 2, 3], index = ["A", "B", "D"]) # C と D は NaN になる
A    2.0
B    4.0
C    NaN
D    NaN

>>> l + pd.Series([1, 2], index = ["A", "B"]) # 長さが異なっても計算可能
A    2.0
B    4.0
C    NaN
dtype: float64
```

`DataFrame` だと行 (インデックス) と列 (カラム) が同じもの同士で計算されます。

```py
>>> data = pd.DataFrame({
...       "product": ["Apple", "Banana", "Carrot"],
...       "sold": [1, 4, 3],
... })
>>> data + data
        product  sold
0    AppleApple     2
1  BananaBanana     8
2  CarrotCarrot     6
```

どれかの要素で計算ができない場合にはエラーになるので、基本的には `Series` で計算することが多いです。

また、`Series`, `DataFrame` に対する数値との演算は、各要素に対して適用されます。

```py
>>> pd.Series([1, 2, 3], index = ["A", "B", "C"]) * 3 # 各要素の値が 3 倍される
A    3
B    6
C    9
dtype: int64

>>> pd.DataFrame({"sold": [1, 4, 3]}) * 3 # 各要素の値が 3 倍される
   sold
0     3
1    12
2     9
```

### 条件式と条件分岐

ある値より高い場合には 1、そうでない場合は 0 などのダミー変数と言われるものを作る場合があります。
例えば、年収の値に応じて、平均以上なら 1 をとる高収入ダミーなどを作成したりします。

その前に、条件式について説明します。
四則演算と同様、数値、`Series` 同士、`DataFrame` 同士で等しいか `==`、等しくないか `!=` といった判定が可能です。
しかし、四則演算とは異なり `Series` 同士で比較するときには、インデックスが全て同じである必要があります。
なお、`DataFrame` 同士で比較するときには、インデックスとカラムどちらも同じである必要があります。

各要素に対する条件式の結果、`bool` 型が返ってきます。

```py
>>> l = pd.Series([1, 2, 3])
>>> r = pd.Series([5, 2, 3])

>>> l == 3 # 各要素に対して a == 3 かどうかを判別
0    False
1    False
2     True
dtype: bool

>>> l == r # 各要素に対して a == b かどうかを判別
0    False
1     True
2     True
dtype: bool

>>> l != r # 各要素に対して a != b かどうかを判別
0     True
1    False
2    False
dtype: bool
```

また、`bool` 型は `&` や `|` を使った演算が可能で、条件 A かつ条件 B を満たすか判別するには `&` を、条件 A または条件 B を満たすかどうかを判別するには `|` を使います。

例えば、次の商品カテゴリーのデータを例にとり、カテゴリーが果物かつ価格が150 円以下かどうか、カテゴリーが果物または価格が 150 以下かどうかを判別します。

```py
>>> data = pd.DataFrame({
... "product": ["Apple", "Banana", "Carrot", "Milk", "Beef"],
... "price": [120, 200, 150, 200, 500],
... "category": ["fruit", "fruit", "vegetable", "dairy", "meat"],
... })
>>> data
  product  price   category
0   Apple    120      fruit
1  Banana    200      fruit
2  Carrot    150  vegetable
3    Milk    200      dairy
4    Beef    500       meat

>>> data["category"] == "fruit" # 商品カテゴリーが果物
0     True
1     True
2    False
3    False
4    False
Name: category, dtype: bool

>>> data["price"] <= 150 # 価格が 150 円以下
0     True
1    False
2     True
3    False
4    False
Name: price, dtype: bool

>>> (data["category"] == "fruit") & (data["price"] <= 150) # 商品カテゴリーが果物 かつ価格が 150 円以下
0     True
1    False
2    False
3    False
4    False
dtype: bool

>>> (data["category"] == "fruit") | (data["price"] <= 150) # 商品カテゴリーが果物 または価格が 150 円以下
0     True
1     True
2     True
3    False
4    False
dtype: bool
```

`bool` 型同士で `&`, `|` を使うので、条件式を括弧で囲み、先に評価する必要があることに注意して下さい。

これを利用すると、ダミー変数を作成できます。
値が `bool` 型に 1 をかけると `True` は 1 に `False` は 0 にできます。
なので、例えば平均価格より高い商品なら 1 をとり、そうでないなら 0 をとるダミー変数は次のように作ります。

```py
>>> data["price"] >= data["price"].mean() # 平均価格以上かどうか
0    False
1    False
2    False
3    False
4     True
Name: price, dtype: bool

>>> 1 * (data["price"] >= data["price"].mean()) # 高価格ダミー変数
0    0
1    0
2    0
3    0
4    1
Name: price, dtype: int64

```

`data["price"].mean()` は価格の平均値をとるので、`data["price"] >= data["price"].mean()` は各要素を数値と比較していることに等しいです。

この結果を元に新しいダミー変数をデータに追加するならば、次のようにします。

```py
>>> data["high_price_dummy"] = 1 * (data["price"] >= data["price"].mean()) # 高価格ダミーを作成
>>> data
  product  price   category  high_price_dummy
0   Apple    120      fruit                 0
1  Banana    200      fruit                 0
2  Carrot    150  vegetable                 0
3    Milk    200      dairy                 0
4    Beef    500       meat                 1
```

### `isin` メソッド

`Series` の [`isin`](https://pandas.pydata.org/docs/reference/api/pandas.Series.isin.html#pandas.Series.isin) メソッドを使うと、配列内の各要素のどれかを含むかどうかを判別できます。

```py
>>> data["category"].isin(["fruit", "meat"]) # 商品カテゴリーが果物か肉かを判別
0     True
1     True
2    False
3    False
4     True
Name: category, dtype: bool
```

## より高度なフィルタリング

単一の条件によるフィルタリング、例えば商品カテゴリーが果物のデータを参照するには次のようにできました。

```py
>>> data[data["category"] == "fruit"]
  product  price category
0   Apple    120    fruit
1  Banana    200    fruit
```

`data["category"] == "fruit"` は `bool` 型の `Series` です。

```py
>>> data["category"] == "fruit"
0     True
1     True
2    False
3    False
4    False
Name: category, dtype: bool
```

つまり、このフィルタリングの構文は、`True` であるインデックスの `DataFrame` だけを返していることがわかります。
なので、複数の条件を組み合わせたフィルタリングも可能です。
商品カテゴリーが果物 かつ価格が 150 円以下のデータを次のように取得可能です。

```py
>>> data[(data["category"] == "fruit") & (data["price"] <= 150)]
  product  price category
0   Apple    120    fruit
```

また、`numpy` の [`where`](https://numpy.org/doc/stable/reference/generated/numpy.where.html) 関数を使うことで、条件に応じた値を入れることもできます。

`where` 関数は、以下のように使います。

```py
numpy.where(<condition>, <x>, <y>)
```

`<condition>` は条件式、`<x>` は条件式が `True` のインデックスに入れる値、`<y>` は条件式が `False` のインデックスに入れる値を示しています。

```py
>>> import pandas as pd
>>> import numpy as np
>>> df = pd.DataFrame({"price": [100, 200, 300, 400, 500]})
>>> df
   price
0    100
1    200
2    300
3    400
4    500

>>> df["price_category"] = np.where(df["price"] >= df["price"].mean(), "high", "low") # 価格に応じてカテゴリーを追加
>>> df
   price price_category
0    100            low
1    200            low
2    300           high
3    400           high
4    500           high
```

## データの結合

分析用のデータを作る際に、複数のデータソースを組み合わる場合があります。
例えば、1 年毎のデータに分かれているものを複数年分のデータに結合することや、アンケートデータと学校の成績のデータを同じ個人で結合したりします。

`pandas` では [`concat`](https://pandas.pydata.org/docs/reference/api/pandas.concat.html) 関数と [`merge`](https://pandas.pydata.org/docs/reference/api/pandas.merge.html) 関数などを用いてデータの結合をします。
データの長さ (インデックス) を増やすような結合を縦方向に結合すると呼んだりします。
縦の結合には、`concat` 関数を用います。
`concat` 関数は結合したい `Series` または `DataFrame` のシーケンスを引数に入れ、`axis` キーワードで結合の方向を指定します。
デフォルトでは縦方向です。

```py
>>> import pandas as pd

# サンプルデータの作成
>>> df1 = pd.DataFrame({'A': [1, 2, 3], 'B': [4, 5, 6]})
>>> df1
   A  B
0  1  4
1  2  5
2  3  6

>>> df2 = pd.DataFrame({'A': [7, 8, 9], 'C': [10, 11, 12]})
>>> df2
   A   C
0  7  10
1  8  11
2  9  12

>>> pd.concat([df1, df2]) # 縦方向の結合
   A    B     C
0  1  4.0   NaN
1  2  5.0   NaN
2  3  6.0   NaN
0  7  NaN  10.0
1  8  NaN  11.0
2  9  NaN  12.0
```

`df1` の下に `df2` がつけ足されていることがわかります。
また、`df1` にないカラム `C`、`df2` にないカラム `B` はそれぞれ、`NaN` が足されています。

> [!tip] 
> 縦方向に結合した場合、インデックスが重複する場合があります。
> インデックスを新たに振りなおすには、`reset_index` メソッドを用います。

データにカラムを足すような結合は横方向の結合と呼びます。
`concat` でも `axis = "columns"` と指定することでこれが可能です。
インデックスが同じもの同士でデータが結合されます。
しかし、以下のように、カラムの重複が起こることもあります。

```py
>>> pd.concat([df1, df2], axis = "columns")
   A  B  A   C
0  1  4  7  10
1  2  5  8  11
2  3  6  9  12
```

横方向の結合では、基本的に `merge` 関数または `join` メソッドを用います。
`merge` 関数には、結合する 2 つのデータを引数に入れます。
このとき、1 番目の引数に入れたものを左、2 番目の引数に入れたものを右として考えます。

`merge` では結合の種類、結合に使う変数 (キー) を指定します。
指定したキーの値が同じもの同士が、データとして結合されます。
キーのカラム名が同じ場合、`on` キーワードで指定します。
異なる場合は、それぞれ `left_on`, `right_on` キーワードで左、右のデータのキーとなるカラム名を指定します。

以下のデータを見ると、`df1` では `key` というカラムでは `A`, `B`, `C` があり、`df2` では `B`, `C`, `D` があり、一部異なっていることが分かります。

```py
# サンプルデータの作成
>>> df1 = pd.DataFrame({'key': ['A', 'B', 'C'], 'value1': [1, 2, 3]})
>>> df1
  key  value1
0   A      1
1   B      2
2   C      3

>>> df2 = pd.DataFrame({'key': ['B', 'C', 'D'], 'value2': [4, 5, 6]})
>>> df2
  key  value2
0   B      4
1   C      5
2   D      6
```

結合には 4 種類あり、内部結合、外部結合、左結合、右結合があります。
`merge` 関数では `how` キーワードで結合の種類を指定します。
デフォルトでは内部結合が選択されます。

内部 (inner) 結合は、キーが合致したもののみを結果として返します。
したがって、両者のキーで合致しない、`A`, `D` は除外されます。

```py
>>> pd.merge(df1, df2, on = "key") # 内部結合
  key  value1  value2
0   B       2       4
1   C       3       5
```

外部 (outer) 結合は、キーが合致しないものも結果として返します。
したがって、両者のキーで合致しない `A`, `D` も結果に含みますが、`A`, `D` では合致しない部分のデータは欠損値になります。

```py
>>> pd.merge(df1, df2, on = "key", how = "outer") # 外部結合
  key  value1  value2
0   A     1.0     NaN
1   B     2.0     4.0
2   C     3.0     5.0
3   D     NaN     6.0
```

左 (left) 結合は、左のデータを全て残し、キーが合致しないものも結果として返します。
したがって、左のキーに合致しない `D` は除外されますが、右のキーに合致しない `A` は残ります。

```py
>>> pd.merge(df1, df2, on = "key", how = "left")
  key  value1  value2
0   A       1     NaN
1   B       2     4.0
2   C       3     5.0
```

右 (right) 結合は左結合の逆です。

```py
>>> pd.merge(df1, df2, on = "key", how = "right")
  key  value1  value2
0   B     2.0       4
1   C     3.0       5
2   D     NaN       6
```

## ピボットテーブル

以下のような、学生のテストスコアのデータがあるとします。

```py
>>> import pandas as pd
>>> data = pd.DataFrame({
...     '学生ID': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
...     'クラス': ['A', 'A', 'B', 'B', 'A', 'B', 'A', 'B', 'A', 'B'],
...     '教科': ['数学', '国語', '数学', '国語', '数学', '国語', '数学', '国語', '数学', '国語'],
...     '点数': [80, 75, 90, 85, 70, 80, 95, 90, 85, 78]
... })
>>> data
   学生ID クラス  教科  点数
0     1   A  数学  80
1     2   A  国語  75
2     3   B  数学  90
3     4   B  国語  85
4     5   A  数学  70
5     6   B  国語  80
6     7   A  数学  95
7     8   B  国語  90
8     9   A  数学  85
9    10   B  国語  78
```

クラス・教科ごとの平均点数を計算し、クラスごとに得意な教科が異なるかを分析したいとします。
[`pivot_table`](https://pandas.pydata.org/docs/reference/api/pandas.pivot_table.html) メソッドはこのようなクロス集計を行うのに便利です。
`pivot_table` を使うことで、データを 2 つ以上のカテゴリーの組み合わせ毎の集計データに変換します。

`pivot_table` は以下の 4 つのキーワードを用います。
- `values`: 集計するデータのカラム名。
- `index`: ピボットテーブルの行になるキーを指定します（複数指定可能）。
- `columns`: ピボットテーブルの列になるキーを指定します（複数指定可能）。
- `aggfunc`: 適用したい集計関数を指定します。
  - デフォルトでは平均値 `mean` で、他にも合計 `sum`、観測数 `count` などがあります。

以下はコード例です。
B クラスは国語・数学どちらも A クラスよりも平均が高いことが分かります。

```py
>>> data.pivot_table(values='点数', index='クラス', columns='教科')
教科      国語    数学
クラス             
A    75.00  82.5
B    83.25  90.0
```

## データ出力

`pandas` で `read_xxx` 関数で特定のファイルを読み込めたように、`to_xxx` メソッドを用いることで `DataFrame` を特定のファイルとして出力できます。
良く用いられるのが、`to_csv` メソッドで、csv 形式のデータファイルに変換してくれます。
メソッドの引数には、出力先のファイルパスを指定します。
拡張子も忘れずに記載するようにしてください。

以下の例は、作成した `DataFrame` を `sample_output.csv` というファイル名の csv ファイルとして出力しています。

```py
>>> data = pd.DataFrame({
    '学生ID': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
    'クラス': ['A', 'A', 'B', 'B', 'A', 'B', 'A', 'B', 'A', 'B'],
    '教科': ['数学', '国語', '数学', '国語', '数学', '国語', '数学', '国語', '数学', '国語'],
    '点数': [80, 75, 90, 85, 70, 80, 95, 90, 85, 78]
})
>>> data
   学生ID クラス  教科  点数
0     1   A  数学  80
1     2   A  国語  75
2     3   B  数学  90
3     4   B  国語  85
4     5   A  数学  70
5     6   B  国語  80
6     7   A  数学  95
7     8   B  国語  90
8     9   A  数学  85
9    10   B  国語  78
>>> data.to_csv("sample_output.csv") # csv として出力
```

> [!tip]
> インデックスが単なる番号である場合、`index = False` とすることでインデックスの出力を除外できます。

他のデータ形式については、pandas のドキュメントを参考にしてください。
- https://pandas.pydata.org/docs/reference/io.html

## ソート・ランク

[`sort_values`](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.sort_values.html) メソッドは、指定したカラムの値に応じてデータを並び替えます。 

```py
>>> import pandas as pd
>>> df = pd.DataFrame({
...     'name': ['Alice', 'Bob', 'Charlie', 'David', 'Eve'],
...     'age': [13, 14, 12, 15, 13],
...     'score': [85, 62, 75, 88, 90]
... })
>>> df
      name  age  score
0    Alice   13     85
1      Bob   14     62
2  Charlie   12     75
3    David   15     88
4      Eve   13     90

>>> df.sort_values("age") # 年齢でソート
      name  age  score
2  Charlie   12     75
0    Alice   13     85
4      Eve   13     90
1      Bob   14     62
3    David   15     88

>>> df.sort_values("score") # 点数でソート
      name  age  score
1      Bob   14     62
2  Charlie   12     75
0    Alice   13     85
3    David   15     88
4      Eve   13     90
```

`ascending` キーワードでソートを昇順 (ascending order) か降順 (decending order) か指定できます。
デフォルトでは昇順で、値が低い方から高い方に並び替えられます。
`ascending = False` で降順にできます。

```py
>>> df.sort_values("age", ascending=False) # 降順にソート
      name  age  score
3    David   15     88
1      Bob   14     62
0    Alice   13     85
4      Eve   13     90
2  Charlie   12     75
```

[`set_index`](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.set_index.html) は指定したカラムをインデックスにすることができます。

```py
>>> df.set_index("name")
         age  score
name               
Alice     13     85
Bob       14     62
Charlie   12     75
David     15     88
Eve       13     90

>>> df.sort_values("age").set_index("name")
         age  score
name               
Charlie   12     75
Alice     13     85
Eve       13     90
Bob       14     62
David     15     88

```

[`sort_index`](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.sort_index.html) はインデックスに応じてデータをソートします。

```py
>>> df.sort_values("age").set_index("name").sort_index()
         age  score
name               
Alice     13     85
Bob       14     62
Charlie   12     75
David     15     88
Eve       13     90
```

## データの確認・整理につかう関数・メソッド

```py
>>> df = pd.DataFrame([
...     [1, 2, 3],
...     [4, 5, 6],
... ])
>>> df
   0  1  2
0  1  2  3
1  4  5  6
```

`shape` 属性は、`DataFrame` の (行数, 列数) を返します。

```py
>>> df.shape
(2, 3)
```

行数が知りたい場合は組み込み関数の `len` 関数を用います。

```py
>>> len(df)
2
```

`size` 属性は 行数 x 列数、つまりの要素の数を返します。

```py
>>> df.size
6
```

`values` 属性は `Series`, `DataFrame` の要素を `numpy` の `ndarray` というオブジェクトとして返します。

```py
>>> df.values
array([[1, 2, 3],
       [4, 5, 6]])
```

`Series` の [`value_counts`](https://pandas.pydata.org/docs/reference/api/pandas.Series.value_counts.html) メソッドは、データの値毎の頻度を数えてくれます。
簡単なデータのサマリーとして重宝します。

```py
>>> fruits = pd.Series(['Apple', 'Banana', 'Orange', 'Banana', 'Apple', 'Orange', 'Apple', 'Banana'])
>>> fruits
0     Apple
1    Banana
2    Orange
3    Banana
4     Apple
5    Orange
6     Apple
7    Banana
dtype: object

>>> fruits.value_counts()
Apple     3
Banana    3
Orange    2
Name: count, dtype: int64
```

また、意図しないデータが紛れ込んでいないかのを確認するのにも使えます。

```py
>>> pd.Series(['Apple', 'Banana', 'Orange', 'Banana', 'Apple', 'Orange', 'Apple', '-']).value_counts()
Apple     3
Banana    2
Orange    2
-         1
Name: count, dtype: int64
```

`transpose` メソッド、もしくは `T` アクセッサーを使うと、`DataFrame` の行と列を入れ替えることができます。

```py
>>> data = pd.DataFraem({
...     'Name': ['Alice', 'Bob', 'Charlie'],
...     'Age': [13, 14, 12],
...     'Grade': [7, 8, 7]
... })
>>> data
      Name  Age  Grade
0    Alice   13      7
1      Bob   14      8
2  Charlie   12      7

>>> data.transpose()
           0    1        2
Name   Alice  Bob  Charlie
Age       13   14       12
Grade      7    8        7

>>> data.T
           0    1        2
Name   Alice  Bob  Charlie
Age       13   14       12
Grade      7    8        7
```