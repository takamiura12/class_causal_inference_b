
# アウトライン

1.	Python の基礎1
2.	Python の基礎2		
3.	Python の基礎3		
4.	Pandas によるデータ操作		
5.	データ分析①		
6.	データ分析②		
7.	データ分析③		
8.	まとめ


## 1. Python の基礎①
   - 授業計画
   - Python とは
   - なぜ Python?: Python のメリット
   - Python のデメリット
   - プログラミングをどう勉強するか
   - Python のインストール
   - Python の立ち上げ・終了
   - VS Code のインストール
   - VS Code の操作
   - 数値・文字列
   - 四則演算・累乗・Mod
## 2. Python の基礎②
   - 文字列
   - 変数
   - ブール値
   - エラーへの対処
   - プログラムの作成手順
   - スクリプトの作成・実行
   - 標準出力
## 3. Python の基礎③
   - 条件分岐
   - 関数
   - ローカル変数
   - 辞書
   - パス
## 4. Python の基礎④
   - 型
   - オブジェクト
   - リスト
   - タプル
   - ループ文
   - クラス
## 5. Pandas によるデータ処理
   - import 文
   - 外部ライブラリ
   - I/O 処理
   - Series, DataFrame
   - データ操作
   - 記述統計
## 6. データ分析②
   - データ操作演習
## 7. データ分析③
   - グラフ作成
## 8. まとめ
   - 演習(予定)