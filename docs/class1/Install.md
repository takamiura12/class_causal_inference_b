# Python・VSCode インストール (Windows OS)

## Microsoft Store の立ち上げ

- 画面左下の虫眼鏡アイコン 🔍 に "microsoft store" と入力します (入力したらエンターを押します)
	- Microsoft Store のアプリが検索結果にでるので、それをクリック

![](../../assets/class1/Pasted%20image%2020231207145152.png)

## Python のインストール

- Microsoft Store の中央上部の虫眼鏡アイコン🔍がある検索欄に "Python" と入力します
- "Python 3.11" と書かれたものをクリックします

![](../../assets/class1/Pasted%20image%2020231207150100.png)

- "Python 3.11" をクリックすると、右上に "インストール" のボタンがあるので、それをクリックします。
- これでインストールは Python のインストールは完了です。

![](../../assets/class1/Pasted%20image%2020231207150343.png)

- Python を立ち上げてみましょう
- Microsoft Store を検索したときと同じように、画面左下の検索欄に "cmd" と入力します。
	- 検索結果に "コマンドプロンプト" というアプリが出るので、それをクリックします

![](../../assets/class1/Pasted%20image%2020231207150639.png)

- 次のような黒い画面 (コンソール画面) が立ち上がるはずです
 
![](../../assets/class1/Pasted%20image%2020231207151257.png)

- このコンソール画面に "python" と入力します
	- "python3" や "python3.11" でも可能です 
		- 複数のバージョンをインストールしている場合は、"python", "python3" はデフォルトのバージョン (3.XX) が立ち上がります。
		- 別のバージョン (Python 3.YY) をインストールしている場合は、"python 3.YY" でそのバージョンが立ち上がります

![](../../assets/class1/Pasted%20image%2020231207151219.png)

- これで、このコンソール内で Python が起動しました。
- 一旦 Python を終了します
	- 終了するには、`quit()` と入力するか、`Ctrl-Z` (Ctrl キーと Z キーの同時押し) を押してからエンターを押します。
	- コンソールを閉じるには、`exit` と入力するか、右上の `x` ボタンをクリックします
		- Python 起動中にコンソールを終了すると、そのコンソール上で動いていた Python も終了します

## VSCode のインストール

- プログラミングでは、エラーが多く発生します。
	- よくあるエラーがスペルミスで、例えば `zeros` と `zero` という表記ゆれなどです
	- こういったエラーを少なくするための環境を用意してくれるツールとして、統合開発環境 (Integrated Development Environment: IDE) があります。
	- この授業では、Visual Studio Code (VSCode) を用います。
		- VSCode は最も人気のある IDE の一つです
	- Python のインストールと同様、Microsoft Store を開き、検索欄に "Vscode" と入力します。
	- "Visual Studio Code" をクリックし、右上のインストールボタンをクリックすればインストールが完了します。

![](../../assets/class1/Pasted%20image%2020231207144825.png)

- インストールしたら、Microsoft Store を検索したときと同じように、検索欄に "vscode" と入力します
	- 次のようなアイコンが検索画面として出るはずです
	- "Visual Studio Code" をクリックして VSCode を立ち上げます

![](../../assets/class1/Pasted%20image%2020231207160616.png)

- 次のような画面が立ち上がります

![](../../assets/class1/Pasted%20image%2020231207155653.png)

- VS Code は様々な拡張機能が用意されており、自分用のカスタマイズができます
	- まずは VS Code を日本語モードに切り替えます
	- 画面左のアイコンが並んでいるところから、赤く囲われているアイコンをクリックします

![](../../assets/class1/Pasted%20image%2020231207161614.png)

- アイコンをクリックすると、右に小さい検索欄が現れるので、そこに "japan" と入力します
![](../../assets/class1/Pasted%20image%2020231207164008.png)

- "Japanese Language ～" という拡張機能が出てくるので、クリックを押すと詳細画面が開きます
- 詳細画面の `install` ボタンをクリックすると、拡張機能のインストールが行われます
	- インストール後、右下に出てくるプロンプトをクリックするか、再起動すると日本語に切り替わります
- 続けて、Python の拡張機能をインストールします
	- 先ほどと同様、拡張機能の検索欄に "python" と入力します `
- "Python" という拡張機能の詳細画面を開き、インストールを行います

![](../../assets/class1/Pasted%20image%2020231207162349.png)

- ついでに、コーディングがより楽になる `IntelliCode` という拡張機能をインストールすることをおすすめします
	- 拡張機能の検索欄に `Intelli` と入力し、`IntelliCode` をインストールします
![](../../assets/class1/Pasted%20image%2020231207164349.png)

- これで下準備は終了です
	- 様々な人が VS Code の拡張機能を開発しており、便利なツールが他にもあります。興味があれば調べてみてください。
	- ただし、誰でも拡張機能を開発できるので、悪意のあるツールが紛れ込む可能性もありますので、注意してください。

# Python・VS Code のインストール (Mac OS)

- Mac を持っていないので、少し雑な説明になりますが、ご容赦ください。

## Python インストール

- Python インストーラーをダウンロードする ([参考動画 Youtube リンク](https://www.youtube.com/watch?v=inuSUJct5s8&ab_channel=ROHITTECH))
  - Python 公式サイトにアクセスして、`Downloads -> macOS` をクリックします。
    - URL: https://www.python.org/downloads/macos/
  - 11 の最新バージョン (Stable Releases) の `macOS 64-bit universal2 installer` をクリックします。
- ダウンロードしたら、`Open With -> Installer` からインストーラーを起動します
  - インストーラーの指示に従って、Python をインストールします
  - `Update Shell Progile.command`, `Install Certific....command` を `Open With -> Terminal` から実行します
- `Terminal` を開いて、`python3 --version` を実行し、バージョンが正しいことを確認します 
  - Terminal (ターミナル) の開き方: [リンク](https://support.apple.com/ja-jp/guide/terminal/apd5265185d-f365-44cb-8b09-71a064a42125/mac)

## VS Code インストール

- VS Code の公式サイトにアクセスして、`Mac` ボタンから `zip` フォルダをダウンロードします ([参考サイト](https://qiita.com/watamura/items/51c70fbb848e5f956fd6))
  - URL: https://code.visualstudio.com/download
- zip フォルダを解凍して、`Visual Studio Code` というアプリケーションをサイドメニューの `アプリケーション` にドラッグ & ドロップします
  - 認証ボタンが出た人は認証ボタンを押します
- アプリケーションから VS Code が開けることを確認します

# VS Code の使い方

- まずは作業するためのフォルダを開きます
	- `フォルダーを開く` ボタンをクリックするか、画面左上の `ファイル` から `フォルダーを開く` をクリックします

![](../../assets/class1/Pasted%20image%2020231207171818.png)

![](../../assets/class1/Pasted%20image%2020231207172015.png)

- 適当なフォルダをクリックします (ここでは `causal_inference` というフォルダ)

![](../../assets/class1/Pasted%20image%2020231207172342.png)

- 次のような画面が出たら、`はい、作成者を信頼します` をクリックします。

 ![](../../assets/class1/Pasted%20image%2020231207172513.png)

- エクスプローラーという場所に、フォルダの中身が表示されます
	- ファイルマークのアイコンをクリックすると、この小さい窓枠 (Pane) が開閉します
	- 今はフォルダに何もないので、何も表示されません

 ![](../../assets/class1/Pasted%20image%2020231207173651.png)

- 開いたフォルダ内に新しくファイルを作成します
- Pane に表示されているフォルダ名 (`CAUSAL_INFERENCE`) の上にマウスを持っていくと、小さなアイコンが表示されます
![](../../assets/class1/Pasted%20image%2020231207174333.png)
- このアイコンは、左から (1) 新規ファイル作成 (2) 新規フォルダ作成 (3) フォルダ内情報の更新 (4) エクスプローラー内のフォルダの折りたたみです。
- (1) のアイコンをクリックします
- あるいは、画面左上の `ファイル` をクリックし、`新しいファイルを作成` を選択します
![](../../assets/class1/Pasted%20image%2020231207174734.png)

- エクスプローラー内にファイル名を入力する欄が表示されます
	- 適当に、`test.py` というファイルを作成します
	- `.py` は Python の拡張子を表します
	- 拡張子はファイルの種類を表すための記号です
![](../../assets/class1/Pasted%20image%2020231207174944.png)
- ファイル名を入力すると、ファイルが開き右に表示されます。
	- 作成しただけなので、`test.py` には何も書かれていません。

![](../../assets/class1/Pasted%20image%2020231207175317.png)
