# Python チュートリアル続き

## 注

`>>>` や `...` は REPL モードで自動的に表示されるものです。
以下のコードの例で `>>>` や `...` が表示されている行は、入力を表し、その後の `>>>` や `...` がないものは出力結果を表しています。 
コピー & ペーストするときは、`>>>` や `...` の文字は削除して使ってください。

例えば、以下は `1` と入力して、その結果 `1` が出力されたということを示しています。

```py
>>> 1
1
```

## 文字列を使う

1 などの数値や `japan` などの文字列をリテラル (literal) といいます。
文字列リテラルをそのまま入力すると (例えば `japan`)、後ほど説明するように変数名として認識されます。 
文字列(リテラル)自体を扱うには、ダブルクォーテーションか `"` シングルクォーテーション `'` で文字列を囲みます。

```py
>>> "JAPAN"
'JAPAN'
```

```py
>>> 'japan'
'japan'
```

数値も `"` や `'` で囲うとで文字列として評価されます。

```py
>>> "100"
'100'
```

同じ行で複数の文字列を定義すると結合されて 1 つの文字列として認識されます。

```py
>>> "wakayama " "prefecture" 
'wakayama prefecture'
```

文字列同士は `+` で結合することができます。

```py
>>> "wakayama " + "prefecture" 
'wakayama prefecture'
```

文字列と数字は足すことはできません。

```py
>>> "Plus" + 1
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: can only concatenate str (not "int") to str
```

文字列 `*` 整数 (n) とすると、文字列が n 回繰り返されます。

```py
>>> "Hello!" * 3
'Hello!Hello!Hello!'
```

文字列同士の引き算や割り算などはできません。

```py
>>> "two" - "one"
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: unsupported operand type(s) for -: 'str' and 'str'
```

> [!note|label:Question]
> Python を使って次の文字を出力してみましょう
> - programming
> - py と thon の結合文字
> - YES を 5 回繰り返した文字

## ブール値

Python では真偽値を `True`, `False` で表します。

```python
>>> True
True
>>> False
False
```

比較演算子はこの真偽値を返します。
基本的な比較演算子は以下のものがあります。

- `x == y`: `x` は `y` と等しい
- `x != y`: `x` は `y` と等しくない
- `x > y`: `x` は `y` より大きい
- `x >= y`: `x` は `y` より大きいか等しい
- `x < y`: `x` は `y` より小さい
- `x <= y`: `x` は `y` より小さいか等しい

特に `==` は変数定義に使う `=` と誤用しやすいので、注意してください。

```python
>>> 3 == 4
False
>>> 5 > 2
True
```

文字列では大文字・小文字は区別されます。
`>` などの大小関係は順番によって決まります。

```python
>>> 'abc' == 'abc'
True
>>> 'a' < 'b'
True
>>> 'abc' == 'ABC'
False
```

数値と文字列など、異なるものの大小は比較できません。

```python
>>> 3 == 'abc'
False
>>> 3 > 'abc'
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: '>' not supported between instances of 'int' and 'str'
```

> [!note|label:Question]
> Python を使って次の文が正しいか判別しましょう
> - $3$ は $10$ より大きい
> - $8$ は $2$ で割り切れる
> - $4 \times 2$ は $9$ より大きい

## 変数を使って値を保存する

長い計算や、途中式の結果などを保存できると使いまわせて便利です。
任意の文字列 (シンボル) に数字や文字列などのデータを割り当てたものを **変数** (variable) と呼びます。
変数に値を割り当てる (定義する) には、`変数名 = 値` と入力します。
例えば、`apple = 100` とすると、`apple` というシンボルに `100` という数値が割り当てられます。

```py
>>> apple = 100
>>> 
```

変数の値を参照するには、変数名を入力します。

```py
>>> apple
100
```

> [!note]
> 変数は値を格納する「箱」と表現されることが多いですが、付箋のような「ラベル」と考えた方が良いです。
> この違いについては、後に説明します。

変数の値を書き換えるには、変数を再定義します。

```py
>>> apple = 150
>>> apple
150
```

変数名は大文字・小文字が区別されることに注意してください。
定義していない変数名を入力するとエラーになります。

```py
>>> APPLE
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'APPLE' is not defined
```

変数には文字列を割り当てることもできます。
変数の定義には `"` や `'` が必要ないことに注意してください。

```py
>>> here = "wakayama"
>>> here
'wakayama'
```

変数の値の参照は、演算などでも行われます。

```py
>>> apple * 5
750
```

```py
>>> budget = 1000
>>> budget - (apple * 3)
550
```

ただし、文字列リテラルのときのように、同じ行で文字列が割り当てられた変数を並べても結合されずエラーになります。

```py
>>> price = "100"
>>> price "円"
  File "<stdin>", line 1
    price "円"
          ^^^
SyntaxError: invalid syntax
```

> [!tip]
> 日本語のテキストだと、`x = 1` を 1 を `x` に代入すると書かれることがほとんどです。
> 数学でもこの操作は代入として呼ばれるので、数学的な意味で解釈できると誤解しがちです。
> しかし、英語ではプログラミングにおける `x = 1` は assignment (割り当て) で、数学における代入 (substitution) とは異なる概念です。
> 例えば、 Python のチュートリアルの[日本語版](https://docs.python.org/ja/3/tutorial/introduction.html#using-python-as-a-calculator)と[英語版](https://docs.python.org/3/tutorial/introduction.html#using-python-as-a-calculator)の説明を比べてみてください。


プログラミング言語は、現実の模倣や抽象化したものが多く存在するので、身近なものに置き換えると理解しやすいことが多いです。
一方で、慣れ親しんでいることで逆に理解しづらくなるものもあります。
変数の割り当て式はその一つです。
`=` は数式で等号の意味で使われますが、この意味をそのままプログラミング言語でも持っていこうとすると変なことが起こります。

例えば、`x = 1` や `y = x + 1` は、数式として考えても意味が通ります。
では、`x = x + 1` はどうでしょうか。
数式として考えると、この式が成り立つ `x` は特別な場合を除いて存在しませんが、Python ではこれはエラーなく通ります (事前に `x` に数値が割り当てられているならば)。
`x` に 1 が事前に割り当てられているなら、`x = x + 1` は `x` に新たに 2 が割り当てられます。
実際に見てみます。

```py
>>> x = 1
>>> x
1
>>> x = x + 1
>>> x
2
```

先ほど説明した通り、左辺は右辺の値を割り当てる名前を表します。 
右辺は値なので、`x` でなく、`x` に割り当てられている値が参照され (つまり 1)、それが 1 と足されます。
したがって、`x = x + 1` は `x = 2` になります。

> [!note|label:Question]
> 次の変数を定義しましょう
> - A さんの身長 (170 cm)
> - A さんの体重 (65 kg)
> - A さんの BMI (体重 / 身長 (m) の 2 乗)

## 予約語

Python を起動すると、いくつかの名前が定義されます。
これを予約語 (reserved words) あるいはキーワードといいます。
先ほどのような、`True` や `False` は予約語の 1 つです。
予約語は上書きができません。

```py
>>> True = 3
  File "<stdin>", line 1
SyntaxError: cannot assign to True
```

Python の予約語は次を参照してください。

- [Python の予約語](https://docs.python.org/ja/3.11/reference/lexical_analysis.html)

`None` は値が存在しないことを示す予約語で、関数などで良く用います。
関数については後ほど説明します。
`None` の判別には `==` ではなく、`is` を用いることが提唱されています。
`None` ではないことを判別するには、`is not` を用います。

```py
>>> A = 3
>>> A is None
False
>>> A is not None
True
```

## エラーに対処する

プログラミングをしていく中で、エラーは必ず起こります。 
ですが、エラーが出たからといって、気落ちしないでください。
エラーが起きたら、その内容をよく読んでみましょう。
例えば、`LEMON` という変数は定義していないので参照しようとするとエラーになります。

```py
>>> LEMON
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'LEMON' is not defined
```

最初に表示されている `Traceback (most recent call last): File "<stdin>", line 1, in <module>` は、エラーになったときの場所に遡って、どこでエラーになったかを表しています。
ここでは、Python ターミナルなのでこのような結果になっていますが、`.py` ファイルを実行したときに、便利さがわかるはずです。
最後の行は、エラーの種類と、その説明が書いてあります。
`NameError` は定義されていない変数を参照しようとすると起きるエラーです。
そのあとに続く文を読めば、`LEMON` という変数名が定義されていないためエラーになっていることが分かります。
Python で基本的に用意されているエラーは次を参照してください。
- [基本的なエラーの種類](https://docs.python.org/ja/3/library/exceptions.html#bltin-exceptions)

では、このエラーを修正します。

```py
>>> LEMON = 1000
>>> LEMON
1000
```

このように、プログラミングはトライ & エラーの連続です。
教育心理学では、生産的失敗という用語があります。
失敗を糧にすることで学びを促進するのです。
たくさん失敗してください!

エラーの内容を読んでもよくわからなかった場合、エラーの内容をコピーして、検索しましょう。
プログラミングに関する Q & A ウェブサイトとして、[stack overflow](https://stackoverflow.com/) が有名です。
ほとんどの場合、こちらを参照すれば同じような問題を抱えた人がいて、それに対する答えも提供されていると思います(一応、日本語版もありますが、英語版よりコンテンツが少ないです)。
また、 [teratail](https://teratail.com/) という日本のサイトもあります。
調べても答えが得られなかった場合は、上記の 2 つで質問してみるのもよいでしょう。
他にも、モジュールの使い方がいまいちわからない場合、 [Qiita](https://qiita.com/) で検索してみましょう。
そのモジュールを使用した記事があるかもしれません。