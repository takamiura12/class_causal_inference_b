因果推論の講義用スライド・スクリプト用レポジトリ

- 因果推論 B
  - 和歌山大学: 2023 年 Q4

- ドキュメントはこちらのページでも公開されています
  - https://class-causal-inference-b-takamiura12-5162cf059579cee94dcc22dfcb.gitlab.io/

----
フォルダ構成
- docs
  - 使用するドキュメントが講義毎に格納されています
- src
  - 使用するスクリプトが格納されています。

